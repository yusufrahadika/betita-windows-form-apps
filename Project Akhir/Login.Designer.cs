﻿namespace Project_Akhir
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            this.bunifuShadowPanel1 = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.bunifuImageButton2 = new Bunifu.UI.WinForms.BunifuImageButton();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tbMasuk = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btLogin = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuShadowPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuShadowPanel1
            // 
            this.bunifuShadowPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bunifuShadowPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuShadowPanel1.BorderColor = System.Drawing.Color.Gainsboro;
            this.bunifuShadowPanel1.Controls.Add(this.bunifuImageButton2);
            this.bunifuShadowPanel1.Controls.Add(this.bunifuLabel1);
            this.bunifuShadowPanel1.Location = new System.Drawing.Point(-15, 12);
            this.bunifuShadowPanel1.Name = "bunifuShadowPanel1";
            this.bunifuShadowPanel1.PanelColor = System.Drawing.Color.Empty;
            this.bunifuShadowPanel1.ShadowDept = 2;
            this.bunifuShadowPanel1.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel1.Size = new System.Drawing.Size(714, 66);
            this.bunifuShadowPanel1.TabIndex = 2;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.ActiveImage = null;
            this.bunifuImageButton2.AllowAnimations = true;
            this.bunifuImageButton2.AllowBuffering = false;
            this.bunifuImageButton2.AllowZooming = true;
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ErrorImage")));
            this.bunifuImageButton2.FadeWhenInactive = false;
            this.bunifuImageButton2.Flip = Bunifu.UI.WinForms.BunifuImageButton.FlipOrientation.Normal;
            this.bunifuImageButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.ImageLocation = null;
            this.bunifuImageButton2.ImageMargin = 40;
            this.bunifuImageButton2.ImageSize = new System.Drawing.Size(29, 26);
            this.bunifuImageButton2.ImageZoomSize = new System.Drawing.Size(69, 66);
            this.bunifuImageButton2.InitialImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.InitialImage")));
            this.bunifuImageButton2.Location = new System.Drawing.Point(3, -3);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Rotation = 90;
            this.bunifuImageButton2.ShowActiveImage = true;
            this.bunifuImageButton2.ShowCursorChanges = true;
            this.bunifuImageButton2.ShowImageBorders = true;
            this.bunifuImageButton2.ShowSizeMarkers = false;
            this.bunifuImageButton2.Size = new System.Drawing.Size(69, 66);
            this.bunifuImageButton2.TabIndex = 3;
            this.bunifuImageButton2.ToolTipText = "";
            this.bunifuImageButton2.WaitOnLoad = false;
            this.bunifuImageButton2.Zoom = 40;
            this.bunifuImageButton2.ZoomSpeed = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(325, 13);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(105, 40);
            this.bunifuLabel1.TabIndex = 2;
            this.bunifuLabel1.Text = "LOGIN";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // tbMasuk
            // 
            this.tbMasuk.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMasuk.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbMasuk.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbMasuk.BackColor = System.Drawing.Color.White;
            this.tbMasuk.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbMasuk.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbMasuk.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbMasuk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbMasuk.HintForeColor = System.Drawing.Color.Empty;
            this.tbMasuk.HintText = "";
            this.tbMasuk.isPassword = false;
            this.tbMasuk.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbMasuk.LineIdleColor = System.Drawing.Color.Gray;
            this.tbMasuk.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbMasuk.LineThickness = 3;
            this.tbMasuk.Location = new System.Drawing.Point(13, 122);
            this.tbMasuk.Margin = new System.Windows.Forms.Padding(4);
            this.tbMasuk.MaxLength = 32767;
            this.tbMasuk.Name = "tbMasuk";
            this.tbMasuk.Size = new System.Drawing.Size(658, 39);
            this.tbMasuk.TabIndex = 3;
            this.tbMasuk.Text = "Username/Email";
            this.tbMasuk.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbMasuk.Enter += new System.EventHandler(this.tbMasuk_Enter);
            this.tbMasuk.Leave += new System.EventHandler(this.tbMasuk_Leave);
            // 
            // tbPassword
            // 
            this.tbPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbPassword.BackColor = System.Drawing.Color.White;
            this.tbPassword.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbPassword.HintForeColor = System.Drawing.Color.Empty;
            this.tbPassword.HintText = "";
            this.tbPassword.isPassword = false;
            this.tbPassword.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbPassword.LineIdleColor = System.Drawing.Color.Gray;
            this.tbPassword.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbPassword.LineThickness = 3;
            this.tbPassword.Location = new System.Drawing.Point(13, 169);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(4);
            this.tbPassword.MaxLength = 32767;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(658, 39);
            this.tbPassword.TabIndex = 4;
            this.tbPassword.Text = "Password";
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbPassword.Enter += new System.EventHandler(this.tbPassword_Enter);
            this.tbPassword.Leave += new System.EventHandler(this.tbPassword_Leave);
            // 
            // btLogin
            // 
            this.btLogin.AllowToggling = false;
            this.btLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btLogin.AnimationSpeed = 200;
            this.btLogin.AutoGenerateColors = false;
            this.btLogin.BackColor = System.Drawing.Color.Transparent;
            this.btLogin.BackColor1 = System.Drawing.Color.White;
            this.btLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btLogin.BackgroundImage")));
            this.btLogin.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btLogin.ButtonText = "Login";
            this.btLogin.ButtonTextMarginLeft = 0;
            this.btLogin.ColorContrastOnClick = 45;
            this.btLogin.ColorContrastOnHover = 45;
            this.btLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btLogin.CustomizableEdges = borderEdges1;
            this.btLogin.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btLogin.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btLogin.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btLogin.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btLogin.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btLogin.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLogin.ForeColor = System.Drawing.Color.Black;
            this.btLogin.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btLogin.IconMarginLeft = 11;
            this.btLogin.IconPadding = 10;
            this.btLogin.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btLogin.IdleBorderColor = System.Drawing.Color.White;
            this.btLogin.IdleBorderRadius = 3;
            this.btLogin.IdleBorderThickness = 1;
            this.btLogin.IdleFillColor = System.Drawing.Color.White;
            this.btLogin.IdleIconLeftImage = null;
            this.btLogin.IdleIconRightImage = null;
            this.btLogin.IndicateFocus = false;
            this.btLogin.Location = new System.Drawing.Point(286, 231);
            this.btLogin.Name = "btLogin";
            this.btLogin.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btLogin.onHoverState.BorderRadius = 3;
            this.btLogin.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btLogin.onHoverState.BorderThickness = 1;
            this.btLogin.onHoverState.FillColor = System.Drawing.Color.White;
            this.btLogin.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.btLogin.onHoverState.IconLeftImage = null;
            this.btLogin.onHoverState.IconRightImage = null;
            this.btLogin.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.btLogin.OnIdleState.BorderRadius = 3;
            this.btLogin.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btLogin.OnIdleState.BorderThickness = 1;
            this.btLogin.OnIdleState.FillColor = System.Drawing.Color.White;
            this.btLogin.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.btLogin.OnIdleState.IconLeftImage = null;
            this.btLogin.OnIdleState.IconRightImage = null;
            this.btLogin.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.btLogin.OnPressedState.BorderRadius = 3;
            this.btLogin.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btLogin.OnPressedState.BorderThickness = 1;
            this.btLogin.OnPressedState.FillColor = System.Drawing.Color.White;
            this.btLogin.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.btLogin.OnPressedState.IconLeftImage = null;
            this.btLogin.OnPressedState.IconRightImage = null;
            this.btLogin.Size = new System.Drawing.Size(148, 42);
            this.btLogin.TabIndex = 8;
            this.btLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btLogin.TextMarginLeft = 0;
            this.btLogin.UseDefaultRadiusAndThickness = true;
            this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.btLogin);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbMasuk);
            this.Controls.Add(this.bunifuShadowPanel1);
            this.Name = "Login";
            this.Text = "Login";
            this.bunifuShadowPanel1.ResumeLayout(false);
            this.bunifuShadowPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel bunifuShadowPanel1;
        private Bunifu.UI.WinForms.BunifuImageButton bunifuImageButton2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbMasuk;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbPassword;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btLogin;
    }
}
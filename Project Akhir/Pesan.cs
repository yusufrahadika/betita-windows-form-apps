﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Project_Akhir
{
    public partial class Pesan : Form
    {
        // Prepare the connection
        private MySqlConnection databaseConnection = Program.databaseConnection;
        public int jumlahVal = 1;
        private string tbJumlahPlaceholder = "Jumlah Tiket";

        public Pesan()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string query = "SELECT perjalanan.id id_perjalanan, kota_asal, kota_tujuan, waktu_berangkat, waktu_sampai, kereta.id id_kereta, kereta.nama nama_kereta, kereta.kuota_ekonomi, kereta.kuota_bisnis, kereta.kuota_eksekutif, harga_ekonomi, harga_bisnis, harga_eksekutif," +
                "kereta.kuota_ekonomi - SUM(CASE WHEN pesanan_perjalanan.kode_kursi LIKE 'ec%' THEN 1 ELSE 0 END) sisa_kuota_ekonomi, " +
                "kereta.kuota_bisnis - SUM(CASE WHEN pesanan_perjalanan.kode_kursi LIKE 'bu%' THEN 1 ELSE 0 END) sisa_kuota_bisnis, " +
                "kereta.kuota_eksekutif - SUM(CASE WHEN pesanan_perjalanan.kode_kursi LIKE 'ex%' THEN 1 ELSE 0 END) sisa_kuota_eksekutif " +
                "FROM perjalanan LEFT JOIN kereta ON perjalanan.id_kereta = kereta.id LEFT JOIN pesanan ON perjalanan.id = pesanan.id_perjalanan " +
                "LEFT JOIN pesanan_perjalanan ON pesanan_perjalanan.id_pesanan = pesanan.id WHERE perjalanan.kota_asal LIKE @asal " +
                "AND perjalanan.kota_tujuan LIKE @tujuan AND perjalanan.waktu_berangkat BETWEEN @tanggal_awal AND @tanggal_akhir GROUP BY perjalanan.id " +
                "HAVING sisa_kuota_ekonomi >= @jumlah OR sisa_kuota_bisnis >= @jumlah OR sisa_kuota_eksekutif >= @jumlah";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@asal", "%" + (cbAsal.Text.Equals("Stasiun Asal") ? "" : cbAsal.Text) + "%");
                cmd.Parameters.AddWithValue("@tujuan", "%" + (cbTujuan.Text.Equals("Stasiun Tujuan") ? "" : cbTujuan.Text) + "%");
                cmd.Parameters.AddWithValue("@tanggal_awal", dtWaktu.Value.ToString("yyyy-MM-dd") + " 00:00:00");
                cmd.Parameters.AddWithValue("@tanggal_akhir", dtWaktu.Value.ToString("yyyy-MM-dd") + " 23:59:59");
                cmd.Parameters.AddWithValue("@jumlah", tbJumlah.Text);

                MySqlDataReader reader = cmd.ExecuteReader();
                // IMPORTANT :
                // If your query returns result, use the following processor :
                flowLayoutPanel1.Controls.Clear();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserControlPesan newUc = new UserControlPesan(
                            reader["id_perjalanan"].ToString(),
                            reader["nama_kereta"].ToString() + " (" + reader["id_kereta"].ToString() + ")",
                            reader["kota_asal"].ToString(),
                            reader["kota_tujuan"].ToString(),
                            (DateTime)reader["waktu_berangkat"],
                            (DateTime)reader["waktu_sampai"],
                            "Kuota Eksekutif" + " (" + reader["sisa_kuota_eksekutif"].ToString() + " / " + reader["kuota_eksekutif"].ToString() + ")",
                            "Kuota Bisnis" + " (" + reader["sisa_kuota_bisnis"].ToString() + " / " + reader["kuota_bisnis"].ToString() + ")",
                            "Kuota Ekonomi" + " (" + reader["sisa_kuota_ekonomi"].ToString() + " / " + reader["kuota_ekonomi"].ToString() + ")",
                            this,
                            Convert.ToInt32(reader["harga_ekonomi"]),
                            Convert.ToInt32(reader["harga_bisnis"]),
                            Convert.ToInt32(reader["harga_eksekutif"])
                        );
                        flowLayoutPanel1.Controls.Add(newUc);
                    }
                    reader.Close();
                }
                else
                {
                    MessageBox.Show("Data perjalanan tidak ditemukan!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        private void tbJumlah_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!(tbJumlah.Text == tbJumlahPlaceholder || tbJumlah.Text == ""))
                {
                    int parsed = int.Parse(tbJumlah.Text);
                    jumlahVal = parsed;
                }
            } catch (Exception)
            {
                tbJumlah.Text = jumlahVal.ToString();
            }
        }

        private void tbJumlah_Enter(object sender, EventArgs e)
        {
            if (tbJumlah.Text == tbJumlahPlaceholder)
            {
                tbJumlah.Text = "";
            }
        }

        private void tbJumlah_Leave(object sender, EventArgs e)
        {
            if (tbJumlah.Text == "")
            {
                tbJumlah.Text = tbJumlahPlaceholder;
                jumlahVal = 1;
            }
        }
    }
}

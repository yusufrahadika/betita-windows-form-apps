﻿namespace Project_Akhir
{
    partial class DataPemesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataPemesan));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.flowLayoutDataPenumpang = new System.Windows.Forms.FlowLayoutPanel();
            this.bunifuButton2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MTEmail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTAlamat = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTNoHandphone = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTNama = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.CLEmail = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.CLAlamat = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.CLNoHandphone = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.CLNama = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btPesan = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.ddKelasKereta = new Bunifu.UI.WinForms.BunifuDropdown();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCards1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuCards1.AutoScroll = true;
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.Tomato;
            this.bunifuCards1.Controls.Add(this.flowLayoutDataPenumpang);
            this.bunifuCards1.Controls.Add(this.bunifuButton2);
            this.bunifuCards1.Controls.Add(this.bunifuSeparator1);
            this.bunifuCards1.Controls.Add(this.groupBox1);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(46, 85);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(623, 283);
            this.bunifuCards1.TabIndex = 35;
            // 
            // flowLayoutDataPenumpang
            // 
            this.flowLayoutDataPenumpang.AutoSize = true;
            this.flowLayoutDataPenumpang.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutDataPenumpang.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutDataPenumpang.Location = new System.Drawing.Point(28, 283);
            this.flowLayoutDataPenumpang.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutDataPenumpang.Name = "flowLayoutDataPenumpang";
            this.flowLayoutDataPenumpang.Size = new System.Drawing.Size(0, 0);
            this.flowLayoutDataPenumpang.TabIndex = 37;
            // 
            // bunifuButton2
            // 
            this.bunifuButton2.AllowToggling = false;
            this.bunifuButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bunifuButton2.AnimationSpeed = 200;
            this.bunifuButton2.AutoGenerateColors = false;
            this.bunifuButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.BackColor1 = System.Drawing.Color.White;
            this.bunifuButton2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton2.BackgroundImage")));
            this.bunifuButton2.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.ButtonText = "Buy Ticket";
            this.bunifuButton2.ButtonTextMarginLeft = 0;
            this.bunifuButton2.CausesValidation = false;
            this.bunifuButton2.ColorContrastOnClick = 45;
            this.bunifuButton2.ColorContrastOnHover = 45;
            this.bunifuButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.bunifuButton2.CustomizableEdges = borderEdges1;
            this.bunifuButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton2.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton2.DisabledFillColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.DisabledForecolor = System.Drawing.Color.Transparent;
            this.bunifuButton2.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton2.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton2.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton2.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton2.IconMarginLeft = 11;
            this.bunifuButton2.IconPadding = 10;
            this.bunifuButton2.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton2.IdleBorderColor = System.Drawing.Color.White;
            this.bunifuButton2.IdleBorderRadius = 3;
            this.bunifuButton2.IdleBorderThickness = 1;
            this.bunifuButton2.IdleFillColor = System.Drawing.Color.White;
            this.bunifuButton2.IdleIconLeftImage = null;
            this.bunifuButton2.IdleIconRightImage = null;
            this.bunifuButton2.IndicateFocus = false;
            this.bunifuButton2.Location = new System.Drawing.Point(244, 554);
            this.bunifuButton2.Name = "bunifuButton2";
            this.bunifuButton2.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuButton2.onHoverState.BorderRadius = 3;
            this.bunifuButton2.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.onHoverState.BorderThickness = 1;
            this.bunifuButton2.onHoverState.FillColor = System.Drawing.Color.White;
            this.bunifuButton2.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton2.onHoverState.IconLeftImage = null;
            this.bunifuButton2.onHoverState.IconRightImage = null;
            this.bunifuButton2.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.bunifuButton2.OnIdleState.BorderRadius = 3;
            this.bunifuButton2.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.OnIdleState.BorderThickness = 1;
            this.bunifuButton2.OnIdleState.FillColor = System.Drawing.Color.White;
            this.bunifuButton2.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton2.OnIdleState.IconLeftImage = null;
            this.bunifuButton2.OnIdleState.IconRightImage = null;
            this.bunifuButton2.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.bunifuButton2.OnPressedState.BorderRadius = 3;
            this.bunifuButton2.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton2.OnPressedState.BorderThickness = 1;
            this.bunifuButton2.OnPressedState.FillColor = System.Drawing.Color.White;
            this.bunifuButton2.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton2.OnPressedState.IconLeftImage = null;
            this.bunifuButton2.OnPressedState.IconRightImage = null;
            this.bunifuButton2.Size = new System.Drawing.Size(148, 42);
            this.bunifuButton2.TabIndex = 36;
            this.bunifuButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton2.TextMarginLeft = 0;
            this.bunifuButton2.UseDefaultRadiusAndThickness = true;
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(28, 214);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(555, 19);
            this.bunifuSeparator1.TabIndex = 35;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.MTEmail);
            this.groupBox1.Controls.Add(this.MTAlamat);
            this.groupBox1.Controls.Add(this.MTNoHandphone);
            this.groupBox1.Controls.Add(this.MTNama);
            this.groupBox1.Controls.Add(this.CLEmail);
            this.groupBox1.Controls.Add(this.CLAlamat);
            this.groupBox1.Controls.Add(this.CLNoHandphone);
            this.groupBox1.Controls.Add(this.CLNama);
            this.groupBox1.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(555, 209);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Pemesan";
            // 
            // MTEmail
            // 
            this.MTEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTEmail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTEmail.Enabled = false;
            this.MTEmail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTEmail.HintForeColor = System.Drawing.Color.Empty;
            this.MTEmail.HintText = "";
            this.MTEmail.isPassword = false;
            this.MTEmail.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTEmail.LineIdleColor = System.Drawing.Color.Gray;
            this.MTEmail.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTEmail.LineThickness = 3;
            this.MTEmail.Location = new System.Drawing.Point(218, 148);
            this.MTEmail.Margin = new System.Windows.Forms.Padding(4);
            this.MTEmail.MaxLength = 32767;
            this.MTEmail.Name = "MTEmail";
            this.MTEmail.Size = new System.Drawing.Size(293, 31);
            this.MTEmail.TabIndex = 38;
            this.MTEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTAlamat
            // 
            this.MTAlamat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTAlamat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTAlamat.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTAlamat.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTAlamat.Enabled = false;
            this.MTAlamat.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTAlamat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTAlamat.HintForeColor = System.Drawing.Color.Empty;
            this.MTAlamat.HintText = "";
            this.MTAlamat.isPassword = false;
            this.MTAlamat.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTAlamat.LineIdleColor = System.Drawing.Color.Gray;
            this.MTAlamat.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTAlamat.LineThickness = 3;
            this.MTAlamat.Location = new System.Drawing.Point(218, 109);
            this.MTAlamat.Margin = new System.Windows.Forms.Padding(4);
            this.MTAlamat.MaxLength = 32767;
            this.MTAlamat.Name = "MTAlamat";
            this.MTAlamat.Size = new System.Drawing.Size(293, 31);
            this.MTAlamat.TabIndex = 37;
            this.MTAlamat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTNoHandphone
            // 
            this.MTNoHandphone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTNoHandphone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTNoHandphone.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTNoHandphone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTNoHandphone.Enabled = false;
            this.MTNoHandphone.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTNoHandphone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTNoHandphone.HintForeColor = System.Drawing.Color.Empty;
            this.MTNoHandphone.HintText = "";
            this.MTNoHandphone.isPassword = false;
            this.MTNoHandphone.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTNoHandphone.LineIdleColor = System.Drawing.Color.Gray;
            this.MTNoHandphone.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTNoHandphone.LineThickness = 3;
            this.MTNoHandphone.Location = new System.Drawing.Point(218, 69);
            this.MTNoHandphone.Margin = new System.Windows.Forms.Padding(4);
            this.MTNoHandphone.MaxLength = 32767;
            this.MTNoHandphone.Name = "MTNoHandphone";
            this.MTNoHandphone.Size = new System.Drawing.Size(293, 32);
            this.MTNoHandphone.TabIndex = 36;
            this.MTNoHandphone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTNama
            // 
            this.MTNama.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTNama.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTNama.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTNama.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTNama.Enabled = false;
            this.MTNama.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTNama.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTNama.HintForeColor = System.Drawing.Color.Empty;
            this.MTNama.HintText = "";
            this.MTNama.isPassword = false;
            this.MTNama.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTNama.LineIdleColor = System.Drawing.Color.Gray;
            this.MTNama.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTNama.LineThickness = 3;
            this.MTNama.Location = new System.Drawing.Point(218, 31);
            this.MTNama.Margin = new System.Windows.Forms.Padding(4);
            this.MTNama.MaxLength = 32767;
            this.MTNama.Name = "MTNama";
            this.MTNama.Size = new System.Drawing.Size(293, 30);
            this.MTNama.TabIndex = 35;
            this.MTNama.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // CLEmail
            // 
            this.CLEmail.AutoSize = true;
            this.CLEmail.BackColor = System.Drawing.Color.Transparent;
            this.CLEmail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLEmail.Location = new System.Drawing.Point(34, 158);
            this.CLEmail.Name = "CLEmail";
            this.CLEmail.Size = new System.Drawing.Size(48, 21);
            this.CLEmail.TabIndex = 34;
            this.CLEmail.Text = "Email";
            // 
            // CLAlamat
            // 
            this.CLAlamat.AutoSize = true;
            this.CLAlamat.BackColor = System.Drawing.Color.Transparent;
            this.CLAlamat.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLAlamat.Location = new System.Drawing.Point(34, 119);
            this.CLAlamat.Name = "CLAlamat";
            this.CLAlamat.Size = new System.Drawing.Size(61, 21);
            this.CLAlamat.TabIndex = 33;
            this.CLAlamat.Text = "Alamat";
            // 
            // CLNoHandphone
            // 
            this.CLNoHandphone.AutoSize = true;
            this.CLNoHandphone.BackColor = System.Drawing.Color.Transparent;
            this.CLNoHandphone.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLNoHandphone.Location = new System.Drawing.Point(34, 80);
            this.CLNoHandphone.Name = "CLNoHandphone";
            this.CLNoHandphone.Size = new System.Drawing.Size(124, 21);
            this.CLNoHandphone.TabIndex = 32;
            this.CLNoHandphone.Text = "No Handphone";
            // 
            // CLNama
            // 
            this.CLNama.AutoSize = true;
            this.CLNama.BackColor = System.Drawing.Color.Transparent;
            this.CLNama.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CLNama.Location = new System.Drawing.Point(34, 40);
            this.CLNama.Name = "CLNama";
            this.CLNama.Size = new System.Drawing.Size(52, 21);
            this.CLNama.TabIndex = 31;
            this.CLNama.Text = "Nama";
            // 
            // btPesan
            // 
            this.btPesan.AllowToggling = false;
            this.btPesan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btPesan.AnimationSpeed = 200;
            this.btPesan.AutoGenerateColors = false;
            this.btPesan.BackColor = System.Drawing.Color.Transparent;
            this.btPesan.BackColor1 = System.Drawing.Color.White;
            this.btPesan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btPesan.BackgroundImage")));
            this.btPesan.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btPesan.ButtonText = "Pesan";
            this.btPesan.ButtonTextMarginLeft = 0;
            this.btPesan.ColorContrastOnClick = 45;
            this.btPesan.ColorContrastOnHover = 45;
            this.btPesan.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.btPesan.CustomizableEdges = borderEdges2;
            this.btPesan.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btPesan.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btPesan.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btPesan.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btPesan.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btPesan.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btPesan.ForeColor = System.Drawing.Color.Black;
            this.btPesan.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btPesan.IconMarginLeft = 11;
            this.btPesan.IconPadding = 10;
            this.btPesan.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btPesan.IdleBorderColor = System.Drawing.Color.White;
            this.btPesan.IdleBorderRadius = 3;
            this.btPesan.IdleBorderThickness = 1;
            this.btPesan.IdleFillColor = System.Drawing.Color.White;
            this.btPesan.IdleIconLeftImage = null;
            this.btPesan.IdleIconRightImage = null;
            this.btPesan.IndicateFocus = false;
            this.btPesan.Location = new System.Drawing.Point(330, 373);
            this.btPesan.Name = "btPesan";
            this.btPesan.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btPesan.onHoverState.BorderRadius = 3;
            this.btPesan.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btPesan.onHoverState.BorderThickness = 1;
            this.btPesan.onHoverState.FillColor = System.Drawing.Color.White;
            this.btPesan.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.btPesan.onHoverState.IconLeftImage = null;
            this.btPesan.onHoverState.IconRightImage = null;
            this.btPesan.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.btPesan.OnIdleState.BorderRadius = 3;
            this.btPesan.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btPesan.OnIdleState.BorderThickness = 1;
            this.btPesan.OnIdleState.FillColor = System.Drawing.Color.White;
            this.btPesan.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.btPesan.OnIdleState.IconLeftImage = null;
            this.btPesan.OnIdleState.IconRightImage = null;
            this.btPesan.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.btPesan.OnPressedState.BorderRadius = 3;
            this.btPesan.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btPesan.OnPressedState.BorderThickness = 1;
            this.btPesan.OnPressedState.FillColor = System.Drawing.Color.White;
            this.btPesan.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.btPesan.OnPressedState.IconLeftImage = null;
            this.btPesan.OnPressedState.IconRightImage = null;
            this.btPesan.Size = new System.Drawing.Size(78, 38);
            this.btPesan.TabIndex = 36;
            this.btPesan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btPesan.TextMarginLeft = 0;
            this.btPesan.UseDefaultRadiusAndThickness = true;
            this.btPesan.Click += new System.EventHandler(this.btPesan_Click);
            // 
            // ddKelasKereta
            // 
            this.ddKelasKereta.BackColor = System.Drawing.SystemColors.Control;
            this.ddKelasKereta.BorderRadius = 1;
            this.ddKelasKereta.Color = System.Drawing.Color.Gray;
            this.ddKelasKereta.Direction = Bunifu.UI.WinForms.BunifuDropdown.Directions.Down;
            this.ddKelasKereta.DisabledColor = System.Drawing.Color.Gray;
            this.ddKelasKereta.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ddKelasKereta.DropdownBorderThickness = Bunifu.UI.WinForms.BunifuDropdown.BorderThickness.Thick;
            this.ddKelasKereta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddKelasKereta.DropDownTextAlign = Bunifu.UI.WinForms.BunifuDropdown.TextAlign.Left;
            this.ddKelasKereta.FillDropDown = false;
            this.ddKelasKereta.FillIndicator = false;
            this.ddKelasKereta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ddKelasKereta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddKelasKereta.ForeColor = System.Drawing.Color.Maroon;
            this.ddKelasKereta.FormattingEnabled = true;
            this.ddKelasKereta.Icon = null;
            this.ddKelasKereta.IndicatorColor = System.Drawing.Color.Gray;
            this.ddKelasKereta.IndicatorLocation = Bunifu.UI.WinForms.BunifuDropdown.Indicator.Right;
            this.ddKelasKereta.ItemBackColor = System.Drawing.Color.White;
            this.ddKelasKereta.ItemBorderColor = System.Drawing.Color.White;
            this.ddKelasKereta.ItemForeColor = System.Drawing.Color.Maroon;
            this.ddKelasKereta.ItemHeight = 26;
            this.ddKelasKereta.ItemHighLightColor = System.Drawing.Color.White;
            this.ddKelasKereta.Items.AddRange(new object[] {
            "Ekonomi",
            "Bisnis",
            "Eksekutif"});
            this.ddKelasKereta.Location = new System.Drawing.Point(223, 23);
            this.ddKelasKereta.Name = "ddKelasKereta";
            this.ddKelasKereta.Size = new System.Drawing.Size(269, 32);
            this.ddKelasKereta.TabIndex = 39;
            this.ddKelasKereta.Text = null;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.BackColor = System.Drawing.Color.OrangeRed;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(42, 23);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(155, 24);
            this.bunifuCustomLabel1.TabIndex = 38;
            this.bunifuCustomLabel1.Text = "Pilih Kelas Kereta";
            // 
            // DataPemesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(710, 416);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.ddKelasKereta);
            this.Controls.Add(this.btPesan);
            this.Controls.Add(this.bunifuCards1);
            this.Name = "DataPemesan";
            this.Text = "MyTrip";
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton2;
        private System.Windows.Forms.GroupBox groupBox1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTEmail;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTAlamat;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTNoHandphone;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTNama;
        private Bunifu.Framework.UI.BunifuCustomLabel CLEmail;
        private Bunifu.Framework.UI.BunifuCustomLabel CLAlamat;
        private Bunifu.Framework.UI.BunifuCustomLabel CLNoHandphone;
        private Bunifu.Framework.UI.BunifuCustomLabel CLNama;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btPesan;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutDataPenumpang;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private Bunifu.UI.WinForms.BunifuDropdown ddKelasKereta;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
    }
}
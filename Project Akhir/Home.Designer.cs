﻿namespace Project_Akhir
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            this.btBuyTicket = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuButton1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuRadioButton1 = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.bunifuButton3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.buttonUser = new Bunifu.Framework.UI.BunifuFlatButton();
            this.LabelEdit = new Bunifu.UI.WinForms.BunifuLabel();
            this.SuspendLayout();
            // 
            // btBuyTicket
            // 
            this.btBuyTicket.AllowToggling = false;
            this.btBuyTicket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btBuyTicket.AnimationSpeed = 200;
            this.btBuyTicket.AutoGenerateColors = false;
            this.btBuyTicket.BackColor = System.Drawing.Color.Transparent;
            this.btBuyTicket.BackColor1 = System.Drawing.Color.White;
            this.btBuyTicket.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btBuyTicket.BackgroundImage")));
            this.btBuyTicket.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btBuyTicket.ButtonText = "Buy Ticket";
            this.btBuyTicket.ButtonTextMarginLeft = 0;
            this.btBuyTicket.ColorContrastOnClick = 45;
            this.btBuyTicket.ColorContrastOnHover = 45;
            this.btBuyTicket.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btBuyTicket.CustomizableEdges = borderEdges1;
            this.btBuyTicket.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btBuyTicket.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btBuyTicket.DisabledFillColor = System.Drawing.Color.Transparent;
            this.btBuyTicket.DisabledForecolor = System.Drawing.Color.Transparent;
            this.btBuyTicket.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btBuyTicket.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btBuyTicket.ForeColor = System.Drawing.Color.Black;
            this.btBuyTicket.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btBuyTicket.IconMarginLeft = 11;
            this.btBuyTicket.IconPadding = 10;
            this.btBuyTicket.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btBuyTicket.IdleBorderColor = System.Drawing.Color.White;
            this.btBuyTicket.IdleBorderRadius = 3;
            this.btBuyTicket.IdleBorderThickness = 1;
            this.btBuyTicket.IdleFillColor = System.Drawing.Color.White;
            this.btBuyTicket.IdleIconLeftImage = null;
            this.btBuyTicket.IdleIconRightImage = null;
            this.btBuyTicket.IndicateFocus = false;
            this.btBuyTicket.Location = new System.Drawing.Point(118, 308);
            this.btBuyTicket.Name = "btBuyTicket";
            this.btBuyTicket.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btBuyTicket.onHoverState.BorderRadius = 3;
            this.btBuyTicket.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btBuyTicket.onHoverState.BorderThickness = 1;
            this.btBuyTicket.onHoverState.FillColor = System.Drawing.Color.White;
            this.btBuyTicket.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.btBuyTicket.onHoverState.IconLeftImage = null;
            this.btBuyTicket.onHoverState.IconRightImage = null;
            this.btBuyTicket.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.btBuyTicket.OnIdleState.BorderRadius = 3;
            this.btBuyTicket.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btBuyTicket.OnIdleState.BorderThickness = 1;
            this.btBuyTicket.OnIdleState.FillColor = System.Drawing.Color.White;
            this.btBuyTicket.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.btBuyTicket.OnIdleState.IconLeftImage = null;
            this.btBuyTicket.OnIdleState.IconRightImage = null;
            this.btBuyTicket.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.btBuyTicket.OnPressedState.BorderRadius = 3;
            this.btBuyTicket.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btBuyTicket.OnPressedState.BorderThickness = 1;
            this.btBuyTicket.OnPressedState.FillColor = System.Drawing.Color.White;
            this.btBuyTicket.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.btBuyTicket.OnPressedState.IconLeftImage = null;
            this.btBuyTicket.OnPressedState.IconRightImage = null;
            this.btBuyTicket.Size = new System.Drawing.Size(148, 42);
            this.btBuyTicket.TabIndex = 11;
            this.btBuyTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btBuyTicket.TextMarginLeft = 0;
            this.btBuyTicket.UseDefaultRadiusAndThickness = true;
            this.btBuyTicket.Click += new System.EventHandler(this.btBuyTicket_Click);
            // 
            // bunifuButton1
            // 
            this.bunifuButton1.AllowToggling = false;
            this.bunifuButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuButton1.AnimationSpeed = 200;
            this.bunifuButton1.AutoGenerateColors = false;
            this.bunifuButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.BackColor1 = System.Drawing.Color.White;
            this.bunifuButton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton1.BackgroundImage")));
            this.bunifuButton1.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.ButtonText = "My Trip";
            this.bunifuButton1.ButtonTextMarginLeft = 0;
            this.bunifuButton1.ColorContrastOnClick = 45;
            this.bunifuButton1.ColorContrastOnHover = 45;
            this.bunifuButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges2.BottomLeft = true;
            borderEdges2.BottomRight = true;
            borderEdges2.TopLeft = true;
            borderEdges2.TopRight = true;
            this.bunifuButton1.CustomizableEdges = borderEdges2;
            this.bunifuButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton1.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton1.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton1.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton1.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton1.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton1.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton1.IconMarginLeft = 11;
            this.bunifuButton1.IconPadding = 10;
            this.bunifuButton1.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton1.IdleBorderColor = System.Drawing.Color.White;
            this.bunifuButton1.IdleBorderRadius = 3;
            this.bunifuButton1.IdleBorderThickness = 1;
            this.bunifuButton1.IdleFillColor = System.Drawing.Color.White;
            this.bunifuButton1.IdleIconLeftImage = null;
            this.bunifuButton1.IdleIconRightImage = null;
            this.bunifuButton1.IndicateFocus = false;
            this.bunifuButton1.Location = new System.Drawing.Point(411, 308);
            this.bunifuButton1.Name = "bunifuButton1";
            this.bunifuButton1.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuButton1.onHoverState.BorderRadius = 3;
            this.bunifuButton1.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.onHoverState.BorderThickness = 1;
            this.bunifuButton1.onHoverState.FillColor = System.Drawing.Color.White;
            this.bunifuButton1.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton1.onHoverState.IconLeftImage = null;
            this.bunifuButton1.onHoverState.IconRightImage = null;
            this.bunifuButton1.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.bunifuButton1.OnIdleState.BorderRadius = 3;
            this.bunifuButton1.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.OnIdleState.BorderThickness = 1;
            this.bunifuButton1.OnIdleState.FillColor = System.Drawing.Color.White;
            this.bunifuButton1.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton1.OnIdleState.IconLeftImage = null;
            this.bunifuButton1.OnIdleState.IconRightImage = null;
            this.bunifuButton1.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.OnPressedState.BorderRadius = 3;
            this.bunifuButton1.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton1.OnPressedState.BorderThickness = 1;
            this.bunifuButton1.OnPressedState.FillColor = System.Drawing.Color.White;
            this.bunifuButton1.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.bunifuButton1.OnPressedState.IconLeftImage = null;
            this.bunifuButton1.OnPressedState.IconRightImage = null;
            this.bunifuButton1.Size = new System.Drawing.Size(148, 42);
            this.bunifuButton1.TabIndex = 12;
            this.bunifuButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton1.TextMarginLeft = 0;
            this.bunifuButton1.UseDefaultRadiusAndThickness = true;
            this.bunifuButton1.Click += new System.EventHandler(this.bunifuButton1_Click);
            // 
            // bunifuRadioButton1
            // 
            this.bunifuRadioButton1.BorderThickness = 1;
            this.bunifuRadioButton1.Checked = true;
            this.bunifuRadioButton1.Location = new System.Drawing.Point(118, 191);
            this.bunifuRadioButton1.Name = "bunifuRadioButton1";
            this.bunifuRadioButton1.OutlineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuRadioButton1.OutlineColorUnchecked = System.Drawing.Color.Gray;
            this.bunifuRadioButton1.RadioColor = System.Drawing.Color.DodgerBlue;
            this.bunifuRadioButton1.Size = new System.Drawing.Size(8, 8);
            this.bunifuRadioButton1.TabIndex = 13;
            this.bunifuRadioButton1.Text = null;
            // 
            // bunifuButton3
            // 
            this.bunifuButton3.AllowToggling = false;
            this.bunifuButton3.AnimationSpeed = 200;
            this.bunifuButton3.AutoGenerateColors = false;
            this.bunifuButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton3.BackColor1 = System.Drawing.Color.DodgerBlue;
            this.bunifuButton3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton3.BackgroundImage")));
            this.bunifuButton3.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.ButtonText = "bunifuButton3";
            this.bunifuButton3.ButtonTextMarginLeft = 0;
            this.bunifuButton3.ColorContrastOnClick = 45;
            this.bunifuButton3.ColorContrastOnHover = 45;
            this.bunifuButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges3.BottomLeft = true;
            borderEdges3.BottomRight = true;
            borderEdges3.TopLeft = true;
            borderEdges3.TopRight = true;
            this.bunifuButton3.CustomizableEdges = borderEdges3;
            this.bunifuButton3.DialogResult = System.Windows.Forms.DialogResult.None;
            this.bunifuButton3.DisabledBorderColor = System.Drawing.Color.Empty;
            this.bunifuButton3.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.bunifuButton3.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.bunifuButton3.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.bunifuButton3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F);
            this.bunifuButton3.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton3.IconMarginLeft = 11;
            this.bunifuButton3.IconPadding = 10;
            this.bunifuButton3.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuButton3.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.bunifuButton3.IdleBorderRadius = 3;
            this.bunifuButton3.IdleBorderThickness = 1;
            this.bunifuButton3.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuButton3.IdleIconLeftImage = null;
            this.bunifuButton3.IdleIconRightImage = null;
            this.bunifuButton3.IndicateFocus = false;
            this.bunifuButton3.Location = new System.Drawing.Point(109, 140);
            this.bunifuButton3.Name = "bunifuButton3";
            this.bunifuButton3.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton3.onHoverState.BorderRadius = 3;
            this.bunifuButton3.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.onHoverState.BorderThickness = 1;
            this.bunifuButton3.onHoverState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(181)))), ((int)(((byte)(255)))));
            this.bunifuButton3.onHoverState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.onHoverState.IconLeftImage = null;
            this.bunifuButton3.onHoverState.IconRightImage = null;
            this.bunifuButton3.OnIdleState.BorderColor = System.Drawing.Color.DodgerBlue;
            this.bunifuButton3.OnIdleState.BorderRadius = 3;
            this.bunifuButton3.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.OnIdleState.BorderThickness = 1;
            this.bunifuButton3.OnIdleState.FillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuButton3.OnIdleState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.OnIdleState.IconLeftImage = null;
            this.bunifuButton3.OnIdleState.IconRightImage = null;
            this.bunifuButton3.OnPressedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton3.OnPressedState.BorderRadius = 3;
            this.bunifuButton3.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.bunifuButton3.OnPressedState.BorderThickness = 1;
            this.bunifuButton3.OnPressedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(96)))), ((int)(((byte)(144)))));
            this.bunifuButton3.OnPressedState.ForeColor = System.Drawing.Color.White;
            this.bunifuButton3.OnPressedState.IconLeftImage = null;
            this.bunifuButton3.OnPressedState.IconRightImage = null;
            this.bunifuButton3.Size = new System.Drawing.Size(9, 8);
            this.bunifuButton3.TabIndex = 14;
            this.bunifuButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuButton3.TextMarginLeft = 0;
            this.bunifuButton3.UseDefaultRadiusAndThickness = true;
            // 
            // buttonUser
            // 
            this.buttonUser.Active = false;
            this.buttonUser.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.buttonUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.buttonUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonUser.BorderRadius = 0;
            this.buttonUser.ButtonText = "Hi, kamu!";
            this.buttonUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonUser.DisabledColor = System.Drawing.Color.Gray;
            this.buttonUser.Iconcolor = System.Drawing.Color.Transparent;
            this.buttonUser.Iconimage = null;
            this.buttonUser.Iconimage_right = ((System.Drawing.Image)(resources.GetObject("buttonUser.Iconimage_right")));
            this.buttonUser.Iconimage_right_Selected = null;
            this.buttonUser.Iconimage_Selected = null;
            this.buttonUser.IconMarginLeft = 0;
            this.buttonUser.IconMarginRight = 0;
            this.buttonUser.IconRightVisible = true;
            this.buttonUser.IconRightZoom = 0D;
            this.buttonUser.IconVisible = true;
            this.buttonUser.IconZoom = 90D;
            this.buttonUser.IsTab = false;
            this.buttonUser.Location = new System.Drawing.Point(526, 25);
            this.buttonUser.Name = "buttonUser";
            this.buttonUser.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.buttonUser.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.buttonUser.OnHoverTextColor = System.Drawing.Color.White;
            this.buttonUser.selected = false;
            this.buttonUser.Size = new System.Drawing.Size(132, 50);
            this.buttonUser.TabIndex = 15;
            this.buttonUser.Text = "Hi, kamu!";
            this.buttonUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonUser.Textcolor = System.Drawing.Color.Black;
            this.buttonUser.TextFont = new System.Drawing.Font("Book Antiqua", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // LabelEdit
            // 
            this.LabelEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelEdit.AutoEllipsis = false;
            this.LabelEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelEdit.CursorType = System.Windows.Forms.Cursors.Hand;
            this.LabelEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelEdit.Location = new System.Drawing.Point(579, 90);
            this.LabelEdit.Name = "LabelEdit";
            this.LabelEdit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LabelEdit.Size = new System.Drawing.Size(79, 22);
            this.LabelEdit.TabIndex = 17;
            this.LabelEdit.Text = "Edit Profile";
            this.LabelEdit.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.LabelEdit.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            this.LabelEdit.Click += new System.EventHandler(this.LabelEdit_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(670, 394);
            this.Controls.Add(this.LabelEdit);
            this.Controls.Add(this.buttonUser);
            this.Controls.Add(this.bunifuButton3);
            this.Controls.Add(this.bunifuRadioButton1);
            this.Controls.Add(this.bunifuButton1);
            this.Controls.Add(this.btBuyTicket);
            this.Name = "Home";
            this.Text = "Home";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btBuyTicket;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton1;
        private Bunifu.UI.WinForms.BunifuRadioButton bunifuRadioButton1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton3;
        private Bunifu.Framework.UI.BunifuFlatButton buttonUser;
        private Bunifu.UI.WinForms.BunifuLabel LabelEdit;
    }
}
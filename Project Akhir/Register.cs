﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Project_Akhir
{
    public partial class Register : Form
    {
        // Prepare the connection
        private static string connectionString = "server=remotemysql.com;port=3306;username=WfAX7PkEDY;password=2DuhAVyKVq;database=WfAX7PkEDY;";
        private MySqlConnection databaseConnection = new MySqlConnection(connectionString);
        private string tbFirstNamePlaceholder = "First Name";
        private string tbLastNamePlaceholder = "Last Name";
        private string tbUsernamePlaceholder = "Username";
        private string tbEmailPlaceholder = "Email";
        private string tbTelephonePlaceholder = "Telephone";
        private string tbAddressPlaceholder = "Address";
        private string tbPasswordPlaceholder = "Password";
        private string tbConfirmPasswordPlaceholder = "Confirm Password";

        Start frmParent = null;
        public Register(Start start)
        {
            frmParent = start;
            InitializeComponent();
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            frmParent.Show();
            this.Hide();
        }

        private void btRegister_Click(object sender, EventArgs e)
        {
            string query = "INSERT INTO pengguna (username, nama_depan, nama_belakang, email, password, nomor_telepon, alamat) VALUES (@username, @nama_depan, @nama_belakang, @email, @password, @nomor_telepon, @alamat)";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@username", tbUsername.Text);
                cmd.Parameters.AddWithValue("@nama_depan", tbFirstName.Text);
                cmd.Parameters.AddWithValue("@nama_belakang", tbLastName.Text);
                cmd.Parameters.AddWithValue("@email", tbEmail.Text);
                cmd.Parameters.AddWithValue("@password", BCrypt.Net.BCrypt.HashPassword(tbPassword.Text));
                cmd.Parameters.AddWithValue("@nomor_telepon", tbTelephone.Text);
                cmd.Parameters.AddWithValue("@alamat", tbAddress.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Register successful!");
                Login login = new Login(frmParent);
                login.Show();
                this.Close();
                tbUsername.ResetText();
                tbFirstName.ResetText();
                tbLastName.ResetText();
                tbEmail.ResetText();
                tbPassword.ResetText();
                tbTelephone.ResetText();
                tbAddress.ResetText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        private void tbFirstName_Enter(object sender, EventArgs e)
        {
            if (tbFirstName.Text == tbFirstNamePlaceholder)
            {
                tbFirstName.Text = "";
            }
        }

        private void tbFirstName_Leave(object sender, EventArgs e)
        {
            if (tbFirstName.Text == "")
            {
                tbFirstName.Text = tbFirstNamePlaceholder;
            }
        }

        private void tbUsername_Enter(object sender, EventArgs e)
        {
            if (tbUsername.Text == tbUsernamePlaceholder)
            {
                tbUsername.Text = "";
            }
        }

        private void tbUsername_Leave(object sender, EventArgs e)
        {
            if (tbUsername.Text == "")
            {
                tbUsername.Text = tbUsernamePlaceholder;
            }
        }

        private void tbLastName_Enter(object sender, EventArgs e)
        {
            if (tbLastName.Text == tbLastNamePlaceholder)
            {
                tbLastName.Text = "";
            }
        }

        private void tbLastName_Leave(object sender, EventArgs e)
        {
            if (tbLastName.Text == "")
            {
                tbLastName.Text = tbLastNamePlaceholder;
            }
        }

        private void tbEmail_Enter(object sender, EventArgs e)
        {
            if (tbEmail.Text == tbEmailPlaceholder)
            {
                tbEmail.Text = "";
            }
        }

        private void tbEmail_Leave(object sender, EventArgs e)
        {
            if (tbEmail.Text == "")
            {
                tbEmail.Text = tbEmailPlaceholder;
            }
        }

        private void tbTelephone_Enter(object sender, EventArgs e)
        {
            if (tbTelephone.Text == tbTelephonePlaceholder)
            {
                tbTelephone.Text = "";
            }
        }

        private void tbTelephone_Leave(object sender, EventArgs e)
        {
            if (tbTelephone.Text == "")
            {
                tbTelephone.Text = tbTelephonePlaceholder;
            }
        }

        private void tbAddress_Enter(object sender, EventArgs e)
        {
            if (tbAddress.Text == tbAddressPlaceholder)
            {
                tbAddress.Text = "";
            }
        }

        private void tbAddress_Leave(object sender, EventArgs e)
        {
            if (tbAddress.Text == "")
            {
                tbAddress.Text = tbAddressPlaceholder;
            }
        }

        private void tbPassword_Enter(object sender, EventArgs e)
        {
            if (tbPassword.Text == tbPasswordPlaceholder)
            {
                tbPassword.Text = "";
                tbPassword.isPassword = true;

            }
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (tbPassword.Text == "")
            {
                tbPassword.Text = tbPasswordPlaceholder;
                tbPassword.isPassword = false;

            }
        }

        private void tbConfirmPassword_Enter(object sender, EventArgs e)
        {
            if (tbConfirmPassword.Text == tbConfirmPasswordPlaceholder)
            {
                tbConfirmPassword.Text = "";
                tbConfirmPassword.isPassword = true;
            }
            
        }

        private void tbConfirmPassword_Leave(object sender, EventArgs e)
        {
            if (tbConfirmPassword.Text == "")
            {
                tbConfirmPassword.Text = tbConfirmPasswordPlaceholder;
                tbConfirmPassword.isPassword = false;

            }
        }
    }
}

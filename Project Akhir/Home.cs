﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Project_Akhir
{
    public partial class Home : Form
    {
        private MySqlConnection databaseConnection = Program.databaseConnection;

        public Home()
        {
            InitializeComponent();
            buttonUser.Text = "Hi, " + Program.nama_depan + "!";
        }

        private void btBuyTicket_Click(object sender, EventArgs e)
        {
            Pesan pesan = new Pesan();
            pesan.Show();
            this.Hide();
        }

        private void LabelEdit_Click(object sender, EventArgs e)
        {
            EditProfile edit = new EditProfile(this);
            edit.Show();
            this.Hide();
        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            MyTrip myTrip = new MyTrip();
            myTrip.Show();
            this.Hide();
        }
    }
}

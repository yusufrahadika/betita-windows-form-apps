﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Akhir
{
    public partial class UserControlPesan : UserControl
    {
        public Pesan pesan;
        public string id_perjalanan;
        public int harga_ekonomi, harga_bisnis, harga_eksekutif;

        public UserControlPesan(
            string id_perjalanan,
            string namaKereta,
            string stasiun_asal,
            string stasiun_akhir,
            DateTime waktu_berangkat,
            DateTime waktu_sampai,
            string eksekutif,
            string bisnis,
            string ekonomi,
            Pesan pesan,
            int harga_ekonomi,
            int harga_bisnis,
            int harga_eksekutif
        )
        {
            InitializeComponent();
            this.id_perjalanan = id_perjalanan;
            labelNamaKereta.Text = namaKereta;
            labelTempatBerangkat.Text = stasiun_asal;
            labelTempatTujuan.Text = stasiun_akhir;
            labelTanggalBerangkat.Text = waktu_berangkat.ToString("dd-MM-yyyy");
            labelTanggalTujuan.Text = waktu_sampai.ToString("dd-MM-yyyy");
            labelWaktuBerangkat.Text = waktu_berangkat.ToString("t");
            labelWaktuTujuan.Text = waktu_sampai.ToString("t");
            labelKuotaEksekutif.Text = eksekutif;
            labelKuotaBisnis.Text = bisnis;
            labelKuotaEkonomi.Text = ekonomi;
            this.pesan = pesan;
            this.harga_ekonomi = harga_ekonomi;
            this.harga_bisnis = harga_bisnis;
            this.harga_eksekutif = harga_eksekutif;
        }

        private void UserControlPesan_Click(object sender, EventArgs e)
        {
            //DataPemesan pemesan = new DataPemesan();
            //pemesan.Show();
            //this.Hide();
        }

        private void LabelNamaKereta_DoubleClick(object sender, EventArgs e)
        {
            DataPemesan pemesan = new DataPemesan(id_perjalanan, pesan.jumlahVal, harga_ekonomi, harga_bisnis, harga_eksekutif, labelTempatBerangkat.Text, labelTempatTujuan.Text);
            pemesan.Show();
        }
    }
}

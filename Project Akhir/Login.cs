﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Project_Akhir
{
    public partial class Login : Form
    {
        // Prepare the connection
        private MySqlConnection databaseConnection = Program.databaseConnection;
        private string tbMasukPlaceholder = "Username/Email";
        private string tbPasswordPlaceholder = "Password";

        Start frmParent = null;
        public Login(Start start)
        {
            frmParent = start;
            InitializeComponent();
        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            frmParent.Show();
            this.Hide();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            string query = "SELECT * FROM pengguna WHERE username = @masuk OR email = @masuk LIMIT 1";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@masuk", tbMasuk.Text);
                MySqlDataReader reader = cmd.ExecuteReader();
                // IMPORTANT :
                // If your query returns result, use the following processor :
                if (reader.HasRows)
                {
                    string hashedPassword = "";
                    while (reader.Read())
                    {
                        hashedPassword = reader["password"].ToString();
                    }
                    if (BCrypt.Net.BCrypt.Verify(tbPassword.Text, hashedPassword))
                    {
                        Program.username = reader["username"].ToString();
                        Program.nama_depan = reader["nama_depan"].ToString();
                        Program.nama_belakang = reader["nama_belakang"].ToString();
                        Program.email = reader["email"].ToString();
                        Program.alamat = reader["alamat"].ToString();
                        Program.nomor_telepon = reader["nomor_telepon"].ToString();
                        Home home = new Home();
                        home.Show();
                        this.Close();
                    } else
                    {
                        MessageBox.Show("Login Failed!");
                    }
                    reader.Close();
                }
                else
                {
                    MessageBox.Show("User not found!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        private void tbMasuk_Enter(object sender, EventArgs e)
        {
            if (tbMasuk.Text == tbMasukPlaceholder)
            {
                tbMasuk.Text = "";
            }
        }

        private void tbMasuk_Leave(object sender, EventArgs e)
        {
            if (tbMasuk.Text == "")
            {
                tbMasuk.Text = tbMasukPlaceholder;
            }
        }

        private void tbPassword_Enter(object sender, EventArgs e)
        {
            if (tbPassword.Text == tbPasswordPlaceholder)
            {
                tbPassword.Text = "";
                tbPassword.isPassword = true;

            }
        }

        private void tbPassword_Leave(object sender, EventArgs e)
        {
            if (tbPassword.Text == "")
            {
                tbPassword.Text = tbPasswordPlaceholder;
                tbPassword.isPassword = false;

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Akhir
{
    public partial class Pembayaran : Form
    {
        public int count, harga;
        public string id_pesanan;

        public Pembayaran(string id_pesanan, int count, int harga)
        {
            InitializeComponent();
            this.count = count;
            this.harga = harga;
            labelHarga.Text = (count * harga).ToString();
            this.id_pesanan = id_pesanan;
        }

        private void bunifuButton1_Click(object sender, EventArgs e)
        {
            FakturPemesan faktur = new FakturPemesan(id_pesanan, count, harga);
            faktur.Show();
            this.Hide();
        }
    }
}

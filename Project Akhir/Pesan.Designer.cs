﻿namespace Project_Akhir
{
    partial class Pesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pesan));
            this.tbJumlah = new System.Windows.Forms.TextBox();
            this.dtWaktu = new System.Windows.Forms.DateTimePicker();
            this.button3 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbAsal = new System.Windows.Forms.ComboBox();
            this.cbTujuan = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbJumlah
            // 
            this.tbJumlah.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJumlah.Location = new System.Drawing.Point(390, 98);
            this.tbJumlah.Name = "tbJumlah";
            this.tbJumlah.Size = new System.Drawing.Size(200, 22);
            this.tbJumlah.TabIndex = 20;
            this.tbJumlah.Text = "Jumlah Tiket";
            this.tbJumlah.TextChanged += new System.EventHandler(this.tbJumlah_TextChanged);
            this.tbJumlah.Enter += new System.EventHandler(this.tbJumlah_Enter);
            this.tbJumlah.Leave += new System.EventHandler(this.tbJumlah_Leave);
            // 
            // dtWaktu
            // 
            this.dtWaktu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtWaktu.Location = new System.Drawing.Point(120, 96);
            this.dtWaktu.Name = "dtWaktu";
            this.dtWaktu.Size = new System.Drawing.Size(200, 22);
            this.dtWaktu.TabIndex = 17;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(120, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(470, 30);
            this.button3.TabIndex = 16;
            this.button3.Text = "Cari";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(120, 162);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(470, 207);
            this.flowLayoutPanel1.TabIndex = 22;
            // 
            // cbAsal
            // 
            this.cbAsal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAsal.FormattingEnabled = true;
            this.cbAsal.Items.AddRange(new object[] {
            "Bandung",
            "Jakarta",
            "Surabaya",
            "Malang",
            "Bogor",
            "Yogyakarta",
            "Semarang",
            "Bali"});
            this.cbAsal.Location = new System.Drawing.Point(120, 55);
            this.cbAsal.Name = "cbAsal";
            this.cbAsal.Size = new System.Drawing.Size(200, 24);
            this.cbAsal.TabIndex = 23;
            this.cbAsal.Text = "Stasiun Asal";
            // 
            // cbTujuan
            // 
            this.cbTujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTujuan.FormattingEnabled = true;
            this.cbTujuan.Items.AddRange(new object[] {
            "Bandung",
            "Jakarta",
            "Surabaya",
            "Malang",
            "Bogor",
            "Yogyakarta",
            "Semarang",
            "Bali"});
            this.cbTujuan.Location = new System.Drawing.Point(390, 55);
            this.cbTujuan.Name = "cbTujuan";
            this.cbTujuan.Size = new System.Drawing.Size(200, 24);
            this.cbTujuan.TabIndex = 24;
            this.cbTujuan.Text = "Stasiun Tujuan";
            // 
            // button5
            // 
            this.button5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button5.BackgroundImage")));
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Location = new System.Drawing.Point(343, 52);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(26, 27);
            this.button5.TabIndex = 25;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // Pesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(710, 378);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.cbTujuan);
            this.Controls.Add(this.cbAsal);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tbJumlah);
            this.Controls.Add(this.dtWaktu);
            this.Controls.Add(this.button3);
            this.Name = "Pesan";
            this.Text = "Pesan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbJumlah;
        private System.Windows.Forms.DateTimePicker dtWaktu;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ComboBox cbAsal;
        private System.Windows.Forms.ComboBox cbTujuan;
        private System.Windows.Forms.Button button5;
    }
}
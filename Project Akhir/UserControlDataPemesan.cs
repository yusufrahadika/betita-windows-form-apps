﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Akhir
{
    public partial class UserControlDataPemesan : UserControl
    {
        public string nama, no_identitas;


        public UserControlDataPemesan(int index)
        {
            InitializeComponent();
            groupBox2.Text += " ";
            groupBox2.Text += index;
        }

        private void tbNoIdentitas_OnValueChanged(object sender, EventArgs e)
        {
            no_identitas = tbNoIdentitas.Text;
        }

        private void tbNama_OnValueChanged(object sender, EventArgs e)
        {
            nama = tbNama.Text;
        }
    }
}

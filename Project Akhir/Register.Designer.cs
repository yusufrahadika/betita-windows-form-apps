﻿namespace Project_Akhir
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            this.bunifuShadowPanel1 = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.bunifuImageButton2 = new Bunifu.UI.WinForms.BunifuImageButton();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.tbFirstName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbLastName = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbUsername = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbEmail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbTelephone = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbAddress = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbConfirmPassword = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btRegister = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuShadowPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuShadowPanel1
            // 
            this.bunifuShadowPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bunifuShadowPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuShadowPanel1.BorderColor = System.Drawing.Color.Gainsboro;
            this.bunifuShadowPanel1.Controls.Add(this.bunifuImageButton2);
            this.bunifuShadowPanel1.Controls.Add(this.bunifuLabel1);
            this.bunifuShadowPanel1.Location = new System.Drawing.Point(-1, 12);
            this.bunifuShadowPanel1.Name = "bunifuShadowPanel1";
            this.bunifuShadowPanel1.PanelColor = System.Drawing.Color.Empty;
            this.bunifuShadowPanel1.ShadowDept = 2;
            this.bunifuShadowPanel1.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel1.Size = new System.Drawing.Size(784, 66);
            this.bunifuShadowPanel1.TabIndex = 3;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.ActiveImage = null;
            this.bunifuImageButton2.AllowAnimations = true;
            this.bunifuImageButton2.AllowBuffering = false;
            this.bunifuImageButton2.AllowZooming = false;
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ErrorImage")));
            this.bunifuImageButton2.FadeWhenInactive = false;
            this.bunifuImageButton2.Flip = Bunifu.UI.WinForms.BunifuImageButton.FlipOrientation.Normal;
            this.bunifuImageButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.ImageLocation = null;
            this.bunifuImageButton2.ImageMargin = 40;
            this.bunifuImageButton2.ImageSize = new System.Drawing.Size(28, 26);
            this.bunifuImageButton2.ImageZoomSize = new System.Drawing.Size(68, 66);
            this.bunifuImageButton2.InitialImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.InitialImage")));
            this.bunifuImageButton2.Location = new System.Drawing.Point(3, 0);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Rotation = 90;
            this.bunifuImageButton2.ShowActiveImage = true;
            this.bunifuImageButton2.ShowCursorChanges = true;
            this.bunifuImageButton2.ShowImageBorders = false;
            this.bunifuImageButton2.ShowSizeMarkers = false;
            this.bunifuImageButton2.Size = new System.Drawing.Size(68, 66);
            this.bunifuImageButton2.TabIndex = 3;
            this.bunifuImageButton2.ToolTipText = "";
            this.bunifuImageButton2.WaitOnLoad = false;
            this.bunifuImageButton2.Zoom = 40;
            this.bunifuImageButton2.ZoomSpeed = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(325, 13);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(125, 31);
            this.bunifuLabel1.TabIndex = 2;
            this.bunifuLabel1.Text = "REGISTER";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tbFirstName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbFirstName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbFirstName.BackColor = System.Drawing.Color.White;
            this.tbFirstName.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbFirstName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbFirstName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbFirstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbFirstName.HintForeColor = System.Drawing.Color.Empty;
            this.tbFirstName.HintText = "";
            this.tbFirstName.isPassword = false;
            this.tbFirstName.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbFirstName.LineIdleColor = System.Drawing.Color.Gray;
            this.tbFirstName.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbFirstName.LineThickness = 3;
            this.tbFirstName.Location = new System.Drawing.Point(13, 127);
            this.tbFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.tbFirstName.MaxLength = 32767;
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(361, 39);
            this.tbFirstName.TabIndex = 4;
            this.tbFirstName.Text = "First Name";
            this.tbFirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbFirstName.Enter += new System.EventHandler(this.tbFirstName_Enter);
            this.tbFirstName.Leave += new System.EventHandler(this.tbFirstName_Leave);
            // 
            // tbLastName
            // 
            this.tbLastName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.tbLastName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbLastName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbLastName.BackColor = System.Drawing.Color.White;
            this.tbLastName.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbLastName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbLastName.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbLastName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbLastName.HintForeColor = System.Drawing.Color.Empty;
            this.tbLastName.HintText = "";
            this.tbLastName.isPassword = false;
            this.tbLastName.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbLastName.LineIdleColor = System.Drawing.Color.Gray;
            this.tbLastName.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbLastName.LineThickness = 3;
            this.tbLastName.Location = new System.Drawing.Point(410, 127);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(4);
            this.tbLastName.MaxLength = 32767;
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(361, 39);
            this.tbLastName.TabIndex = 5;
            this.tbLastName.Text = "Last Name";
            this.tbLastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbLastName.Enter += new System.EventHandler(this.tbLastName_Enter);
            this.tbLastName.Leave += new System.EventHandler(this.tbLastName_Leave);
            // 
            // tbUsername
            // 
            this.tbUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUsername.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbUsername.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbUsername.BackColor = System.Drawing.Color.White;
            this.tbUsername.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbUsername.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbUsername.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbUsername.HintForeColor = System.Drawing.Color.Empty;
            this.tbUsername.HintText = "";
            this.tbUsername.isPassword = false;
            this.tbUsername.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbUsername.LineIdleColor = System.Drawing.Color.Gray;
            this.tbUsername.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbUsername.LineThickness = 3;
            this.tbUsername.Location = new System.Drawing.Point(13, 174);
            this.tbUsername.Margin = new System.Windows.Forms.Padding(4);
            this.tbUsername.MaxLength = 32767;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(758, 39);
            this.tbUsername.TabIndex = 6;
            this.tbUsername.Text = "Username";
            this.tbUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbUsername.Enter += new System.EventHandler(this.tbUsername_Enter);
            this.tbUsername.Leave += new System.EventHandler(this.tbUsername_Leave);
            // 
            // tbEmail
            // 
            this.tbEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbEmail.BackColor = System.Drawing.Color.White;
            this.tbEmail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbEmail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbEmail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbEmail.HintForeColor = System.Drawing.Color.Empty;
            this.tbEmail.HintText = "";
            this.tbEmail.isPassword = false;
            this.tbEmail.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbEmail.LineIdleColor = System.Drawing.Color.Gray;
            this.tbEmail.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbEmail.LineThickness = 3;
            this.tbEmail.Location = new System.Drawing.Point(13, 221);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(4);
            this.tbEmail.MaxLength = 32767;
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(758, 39);
            this.tbEmail.TabIndex = 7;
            this.tbEmail.Text = "Email";
            this.tbEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbEmail.Enter += new System.EventHandler(this.tbEmail_Enter);
            this.tbEmail.Leave += new System.EventHandler(this.tbEmail_Leave);
            // 
            // tbTelephone
            // 
            this.tbTelephone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTelephone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbTelephone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbTelephone.BackColor = System.Drawing.Color.White;
            this.tbTelephone.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbTelephone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbTelephone.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbTelephone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbTelephone.HintForeColor = System.Drawing.Color.Empty;
            this.tbTelephone.HintText = "";
            this.tbTelephone.isPassword = false;
            this.tbTelephone.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbTelephone.LineIdleColor = System.Drawing.Color.Gray;
            this.tbTelephone.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbTelephone.LineThickness = 3;
            this.tbTelephone.Location = new System.Drawing.Point(13, 268);
            this.tbTelephone.Margin = new System.Windows.Forms.Padding(4);
            this.tbTelephone.MaxLength = 32767;
            this.tbTelephone.Name = "tbTelephone";
            this.tbTelephone.Size = new System.Drawing.Size(758, 39);
            this.tbTelephone.TabIndex = 8;
            this.tbTelephone.Text = "Telephone";
            this.tbTelephone.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbTelephone.Enter += new System.EventHandler(this.tbTelephone_Enter);
            this.tbTelephone.Leave += new System.EventHandler(this.tbTelephone_Leave);
            // 
            // tbAddress
            // 
            this.tbAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAddress.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbAddress.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbAddress.BackColor = System.Drawing.Color.White;
            this.tbAddress.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbAddress.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbAddress.HintForeColor = System.Drawing.Color.Empty;
            this.tbAddress.HintText = "";
            this.tbAddress.isPassword = false;
            this.tbAddress.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbAddress.LineIdleColor = System.Drawing.Color.Gray;
            this.tbAddress.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbAddress.LineThickness = 3;
            this.tbAddress.Location = new System.Drawing.Point(13, 315);
            this.tbAddress.Margin = new System.Windows.Forms.Padding(4);
            this.tbAddress.MaxLength = 32767;
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(758, 39);
            this.tbAddress.TabIndex = 9;
            this.tbAddress.Text = "Address";
            this.tbAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbAddress.Enter += new System.EventHandler(this.tbAddress_Enter);
            this.tbAddress.Leave += new System.EventHandler(this.tbAddress_Leave);
            // 
            // tbPassword
            // 
            this.tbPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbPassword.BackColor = System.Drawing.Color.White;
            this.tbPassword.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbPassword.HintForeColor = System.Drawing.Color.Empty;
            this.tbPassword.HintText = "";
            this.tbPassword.isPassword = false;
            this.tbPassword.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbPassword.LineIdleColor = System.Drawing.Color.Gray;
            this.tbPassword.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbPassword.LineThickness = 3;
            this.tbPassword.Location = new System.Drawing.Point(13, 362);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(4);
            this.tbPassword.MaxLength = 32767;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(758, 39);
            this.tbPassword.TabIndex = 10;
            this.tbPassword.Text = "Password";
            this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbPassword.Enter += new System.EventHandler(this.tbPassword_Enter);
            this.tbPassword.Leave += new System.EventHandler(this.tbPassword_Leave);
            // 
            // tbConfirmPassword
            // 
            this.tbConfirmPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbConfirmPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbConfirmPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbConfirmPassword.BackColor = System.Drawing.Color.White;
            this.tbConfirmPassword.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbConfirmPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbConfirmPassword.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbConfirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbConfirmPassword.HintForeColor = System.Drawing.Color.Empty;
            this.tbConfirmPassword.HintText = "";
            this.tbConfirmPassword.isPassword = false;
            this.tbConfirmPassword.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbConfirmPassword.LineIdleColor = System.Drawing.Color.Gray;
            this.tbConfirmPassword.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbConfirmPassword.LineThickness = 3;
            this.tbConfirmPassword.Location = new System.Drawing.Point(13, 409);
            this.tbConfirmPassword.Margin = new System.Windows.Forms.Padding(4);
            this.tbConfirmPassword.MaxLength = 32767;
            this.tbConfirmPassword.Name = "tbConfirmPassword";
            this.tbConfirmPassword.Size = new System.Drawing.Size(758, 39);
            this.tbConfirmPassword.TabIndex = 11;
            this.tbConfirmPassword.Text = "Confirm Password";
            this.tbConfirmPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbConfirmPassword.Enter += new System.EventHandler(this.tbConfirmPassword_Enter);
            this.tbConfirmPassword.Leave += new System.EventHandler(this.tbConfirmPassword_Leave);
            // 
            // btRegister
            // 
            this.btRegister.AllowToggling = false;
            this.btRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btRegister.AnimationSpeed = 200;
            this.btRegister.AutoGenerateColors = false;
            this.btRegister.BackColor = System.Drawing.Color.Transparent;
            this.btRegister.BackColor1 = System.Drawing.Color.White;
            this.btRegister.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btRegister.BackgroundImage")));
            this.btRegister.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btRegister.ButtonText = "Register";
            this.btRegister.ButtonTextMarginLeft = 0;
            this.btRegister.ColorContrastOnClick = 45;
            this.btRegister.ColorContrastOnHover = 45;
            this.btRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btRegister.CustomizableEdges = borderEdges1;
            this.btRegister.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btRegister.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btRegister.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btRegister.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btRegister.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btRegister.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btRegister.ForeColor = System.Drawing.Color.Black;
            this.btRegister.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btRegister.IconMarginLeft = 11;
            this.btRegister.IconPadding = 10;
            this.btRegister.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btRegister.IdleBorderColor = System.Drawing.Color.White;
            this.btRegister.IdleBorderRadius = 3;
            this.btRegister.IdleBorderThickness = 1;
            this.btRegister.IdleFillColor = System.Drawing.Color.White;
            this.btRegister.IdleIconLeftImage = null;
            this.btRegister.IdleIconRightImage = null;
            this.btRegister.IndicateFocus = false;
            this.btRegister.Location = new System.Drawing.Point(318, 455);
            this.btRegister.Name = "btRegister";
            this.btRegister.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btRegister.onHoverState.BorderRadius = 3;
            this.btRegister.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btRegister.onHoverState.BorderThickness = 1;
            this.btRegister.onHoverState.FillColor = System.Drawing.Color.White;
            this.btRegister.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.btRegister.onHoverState.IconLeftImage = null;
            this.btRegister.onHoverState.IconRightImage = null;
            this.btRegister.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.btRegister.OnIdleState.BorderRadius = 3;
            this.btRegister.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btRegister.OnIdleState.BorderThickness = 1;
            this.btRegister.OnIdleState.FillColor = System.Drawing.Color.White;
            this.btRegister.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.btRegister.OnIdleState.IconLeftImage = null;
            this.btRegister.OnIdleState.IconRightImage = null;
            this.btRegister.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.btRegister.OnPressedState.BorderRadius = 3;
            this.btRegister.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btRegister.OnPressedState.BorderThickness = 1;
            this.btRegister.OnPressedState.FillColor = System.Drawing.Color.White;
            this.btRegister.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.btRegister.OnPressedState.IconLeftImage = null;
            this.btRegister.OnPressedState.IconRightImage = null;
            this.btRegister.Size = new System.Drawing.Size(148, 42);
            this.btRegister.TabIndex = 12;
            this.btRegister.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btRegister.TextMarginLeft = 0;
            this.btRegister.UseDefaultRadiusAndThickness = true;
            this.btRegister.Click += new System.EventHandler(this.btRegister_Click);
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 514);
            this.Controls.Add(this.btRegister);
            this.Controls.Add(this.tbConfirmPassword);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.tbTelephone);
            this.Controls.Add(this.tbEmail);
            this.Controls.Add(this.tbUsername);
            this.Controls.Add(this.tbLastName);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.bunifuShadowPanel1);
            this.Name = "Register";
            this.Text = "Register";
            this.bunifuShadowPanel1.ResumeLayout(false);
            this.bunifuShadowPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel bunifuShadowPanel1;
        private Bunifu.UI.WinForms.BunifuImageButton bunifuImageButton2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbFirstName;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbLastName;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbUsername;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbEmail;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbTelephone;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbAddress;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbPassword;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbConfirmPassword;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btRegister;
    }
}
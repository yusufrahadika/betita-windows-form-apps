﻿namespace Project_Akhir
{
    partial class UserControlPesan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlPesan));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelKelas = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelNamaKereta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelTempatBerangkat = new System.Windows.Forms.Label();
            this.labelTanggalBerangkat = new System.Windows.Forms.Label();
            this.labelWaktuBerangkat = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.bunifuImageButton2 = new Bunifu.UI.WinForms.BunifuImageButton();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelTempatTujuan = new System.Windows.Forms.Label();
            this.labelTanggalTujuan = new System.Windows.Forms.Label();
            this.labelWaktuTujuan = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelKuotaEkonomi = new System.Windows.Forms.Label();
            this.labelKuotaBisnis = new System.Windows.Forms.Label();
            this.labelKuotaEksekutif = new System.Windows.Forms.Label();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.labelKelas);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel4);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel5);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(479, 80);
            this.flowLayoutPanel1.TabIndex = 0;
            this.flowLayoutPanel1.DoubleClick += new System.EventHandler(this.LabelNamaKereta_DoubleClick);
            // 
            // labelKelas
            // 
            this.labelKelas.AutoSize = true;
            this.labelKelas.Cursor = System.Windows.Forms.Cursors.No;
            this.labelKelas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKelas.Location = new System.Drawing.Point(3, 0);
            this.labelKelas.Name = "labelKelas";
            this.labelKelas.Size = new System.Drawing.Size(0, 15);
            this.labelKelas.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.labelNamaKereta);
            this.flowLayoutPanel4.Controls.Add(this.label1);
            this.flowLayoutPanel4.Controls.Add(this.labelTempatBerangkat);
            this.flowLayoutPanel4.Controls.Add(this.labelTanggalBerangkat);
            this.flowLayoutPanel4.Controls.Add(this.labelWaktuBerangkat);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(9, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(139, 69);
            this.flowLayoutPanel4.TabIndex = 9;
            // 
            // labelNamaKereta
            // 
            this.labelNamaKereta.AutoSize = true;
            this.labelNamaKereta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNamaKereta.Location = new System.Drawing.Point(3, 0);
            this.labelNamaKereta.Name = "labelNamaKereta";
            this.labelNamaKereta.Size = new System.Drawing.Size(126, 16);
            this.labelNamaKereta.TabIndex = 0;
            this.labelNamaKereta.Text = "MATARAMAJA (24)";
            this.labelNamaKereta.DoubleClick += new System.EventHandler(this.LabelNamaKereta_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 5;
            // 
            // labelTempatBerangkat
            // 
            this.labelTempatBerangkat.AutoSize = true;
            this.labelTempatBerangkat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTempatBerangkat.Location = new System.Drawing.Point(3, 29);
            this.labelTempatBerangkat.Name = "labelTempatBerangkat";
            this.labelTempatBerangkat.Size = new System.Drawing.Size(48, 15);
            this.labelTempatBerangkat.TabIndex = 2;
            this.labelTempatBerangkat.Text = "Gambir";
            // 
            // labelTanggalBerangkat
            // 
            this.labelTanggalBerangkat.AutoSize = true;
            this.labelTanggalBerangkat.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTanggalBerangkat.Location = new System.Drawing.Point(3, 44);
            this.labelTanggalBerangkat.Name = "labelTanggalBerangkat";
            this.labelTanggalBerangkat.Size = new System.Drawing.Size(58, 12);
            this.labelTanggalBerangkat.TabIndex = 3;
            this.labelTanggalBerangkat.Text = "23 Sept 2019";
            // 
            // labelWaktuBerangkat
            // 
            this.labelWaktuBerangkat.AutoSize = true;
            this.labelWaktuBerangkat.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaktuBerangkat.Location = new System.Drawing.Point(3, 56);
            this.labelWaktuBerangkat.Name = "labelWaktuBerangkat";
            this.labelWaktuBerangkat.Size = new System.Drawing.Size(48, 12);
            this.labelWaktuBerangkat.TabIndex = 4;
            this.labelWaktuBerangkat.Text = "07.45 WIB";
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.bunifuImageButton2);
            this.flowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(153, 2);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(39, 62);
            this.flowLayoutPanel5.TabIndex = 10;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.ActiveImage = null;
            this.bunifuImageButton2.AllowAnimations = true;
            this.bunifuImageButton2.AllowBuffering = false;
            this.bunifuImageButton2.AllowZooming = false;
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.BackgroundImage")));
            this.bunifuImageButton2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ErrorImage")));
            this.bunifuImageButton2.FadeWhenInactive = false;
            this.bunifuImageButton2.Flip = Bunifu.UI.WinForms.BunifuImageButton.FlipOrientation.Normal;
            this.bunifuImageButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.ImageLocation = null;
            this.bunifuImageButton2.ImageMargin = 40;
            this.bunifuImageButton2.ImageSize = new System.Drawing.Size(7, 0);
            this.bunifuImageButton2.ImageZoomSize = new System.Drawing.Size(47, 40);
            this.bunifuImageButton2.InitialImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.InitialImage")));
            this.bunifuImageButton2.Location = new System.Drawing.Point(4, 17);
            this.bunifuImageButton2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Rotation = 0;
            this.bunifuImageButton2.ShowActiveImage = true;
            this.bunifuImageButton2.ShowCursorChanges = true;
            this.bunifuImageButton2.ShowImageBorders = false;
            this.bunifuImageButton2.ShowSizeMarkers = false;
            this.bunifuImageButton2.Size = new System.Drawing.Size(47, 40);
            this.bunifuImageButton2.TabIndex = 37;
            this.bunifuImageButton2.ToolTipText = "";
            this.bunifuImageButton2.WaitOnLoad = false;
            this.bunifuImageButton2.Zoom = 40;
            this.bunifuImageButton2.ZoomSpeed = 10;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label11);
            this.flowLayoutPanel2.Controls.Add(this.label10);
            this.flowLayoutPanel2.Controls.Add(this.labelTempatTujuan);
            this.flowLayoutPanel2.Controls.Add(this.labelTanggalTujuan);
            this.flowLayoutPanel2.Controls.Add(this.labelWaktuTujuan);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(197, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(128, 69);
            this.flowLayoutPanel2.TabIndex = 6;
            this.flowLayoutPanel2.DoubleClick += new System.EventHandler(this.LabelNamaKereta_DoubleClick);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 13;
            // 
            // labelTempatTujuan
            // 
            this.labelTempatTujuan.AutoSize = true;
            this.labelTempatTujuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelTempatTujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTempatTujuan.Location = new System.Drawing.Point(3, 26);
            this.labelTempatTujuan.Name = "labelTempatTujuan";
            this.labelTempatTujuan.Size = new System.Drawing.Size(57, 15);
            this.labelTempatTujuan.TabIndex = 9;
            this.labelTempatTujuan.Text = "Bandung";
            // 
            // labelTanggalTujuan
            // 
            this.labelTanggalTujuan.AutoSize = true;
            this.labelTanggalTujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTanggalTujuan.Location = new System.Drawing.Point(3, 41);
            this.labelTanggalTujuan.Name = "labelTanggalTujuan";
            this.labelTanggalTujuan.Size = new System.Drawing.Size(58, 12);
            this.labelTanggalTujuan.TabIndex = 11;
            this.labelTanggalTujuan.Text = "24 Sept 2019";
            // 
            // labelWaktuTujuan
            // 
            this.labelWaktuTujuan.AutoSize = true;
            this.labelWaktuTujuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaktuTujuan.Location = new System.Drawing.Point(3, 53);
            this.labelWaktuTujuan.Name = "labelWaktuTujuan";
            this.labelWaktuTujuan.Size = new System.Drawing.Size(48, 12);
            this.labelWaktuTujuan.TabIndex = 12;
            this.labelWaktuTujuan.Text = "01.30 WIB";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.labelKuotaEkonomi);
            this.flowLayoutPanel3.Controls.Add(this.labelKuotaBisnis);
            this.flowLayoutPanel3.Controls.Add(this.labelKuotaEksekutif);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(331, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(142, 69);
            this.flowLayoutPanel3.TabIndex = 8;
            this.flowLayoutPanel3.DoubleClick += new System.EventHandler(this.LabelNamaKereta_DoubleClick);
            // 
            // labelKuotaEkonomi
            // 
            this.labelKuotaEkonomi.AutoSize = true;
            this.labelKuotaEkonomi.Location = new System.Drawing.Point(3, 56);
            this.labelKuotaEkonomi.Name = "labelKuotaEkonomi";
            this.labelKuotaEkonomi.Size = new System.Drawing.Size(79, 13);
            this.labelKuotaEkonomi.TabIndex = 0;
            this.labelKuotaEkonomi.Text = "Kuota Ekonomi";
            // 
            // labelKuotaBisnis
            // 
            this.labelKuotaBisnis.AutoSize = true;
            this.labelKuotaBisnis.Location = new System.Drawing.Point(3, 43);
            this.labelKuotaBisnis.Name = "labelKuotaBisnis";
            this.labelKuotaBisnis.Size = new System.Drawing.Size(65, 13);
            this.labelKuotaBisnis.TabIndex = 1;
            this.labelKuotaBisnis.Text = "Kuota Bisnis";
            // 
            // labelKuotaEksekutif
            // 
            this.labelKuotaEksekutif.AutoSize = true;
            this.labelKuotaEksekutif.Location = new System.Drawing.Point(3, 30);
            this.labelKuotaEksekutif.Name = "labelKuotaEksekutif";
            this.labelKuotaEksekutif.Size = new System.Drawing.Size(82, 13);
            this.labelKuotaEksekutif.TabIndex = 2;
            this.labelKuotaEksekutif.Text = "Kuota Eksekutif";
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(5, 81);
            this.bunifuSeparator1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(466, 15);
            this.bunifuSeparator1.TabIndex = 36;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // UserControlPesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bunifuSeparator1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UserControlPesan";
            this.Size = new System.Drawing.Size(483, 96);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label labelTempatBerangkat;
        private System.Windows.Forms.Label labelTanggalBerangkat;
        private System.Windows.Forms.Label labelWaktuBerangkat;
        private System.Windows.Forms.Label labelNamaKereta;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelTempatTujuan;
        private System.Windows.Forms.Label labelTanggalTujuan;
        private System.Windows.Forms.Label labelWaktuTujuan;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Label labelKuotaEkonomi;
        private System.Windows.Forms.Label labelKuotaBisnis;
        private System.Windows.Forms.Label labelKuotaEksekutif;
        private System.Windows.Forms.Label labelKelas;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private Bunifu.UI.WinForms.BunifuImageButton bunifuImageButton2;
    }
}

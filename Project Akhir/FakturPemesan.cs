﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace Project_Akhir
{
    public partial class FakturPemesan : Form
    {
        private MySqlConnection databaseConnection = Program.databaseConnection;
        private string id_pesanan;

        public FakturPemesan(string id_pesanan, int count, int harga)
        {
            databaseConnection.Open();
            InitializeComponent();
            MTpemesan.Text = Program.nama_depan + " " + Program.nama_belakang;
            
  
            this.id_pesanan = id_pesanan;
            labelKode.Text = id_pesanan.Substring(0, 8);
            Labeljumlahtiket.Text = count.ToString();
            Labelharga.Text = (count * harga).ToString();

            string query = "SELECT * FROM pesanan JOIN perjalanan on perjalanan.id = pesanan.id_perjalanan where pesanan.id = @id_pesanan";
            MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
            cmd.CommandTimeout = 60;
            cmd.Parameters.AddWithValue("@id_pesanan", id_pesanan);

            MySqlDataReader reader = cmd.ExecuteReader();
            //cmd.Parameters.AddWithValue("@tanggal_awal", dtWaktu.Value.ToString("yyyy-MM-dd") + " 00:00:00");

            //MTtanggal.Text = reader["waktu_berangkat"].ToString("yyyy-MM-dd") + " 00:00:00");
            while (reader.Read())
            {
                MTasal.Text = reader["kota_asal"].ToString();
                MTtujuan.Text = reader["kota_tujuan"].ToString();
            }
                

            databaseConnection.Close();


        }

        private void bunifuImageButton2_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }
    }
}

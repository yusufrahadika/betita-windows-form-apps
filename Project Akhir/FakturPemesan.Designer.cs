﻿namespace Project_Akhir
{
    partial class FakturPemesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FakturPemesan));
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.MTtujuan = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTasal = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTtanggal = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTpemesan = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuLabel5 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bunifuImageButton2 = new Bunifu.UI.WinForms.BunifuImageButton();
            this.labelKode = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel8 = new Bunifu.UI.WinForms.BunifuLabel();
            this.Labeljumlahtiket = new Bunifu.UI.WinForms.BunifuLabel();
            this.Labelharga = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel11 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.Tomato;
            this.bunifuCards1.Controls.Add(this.MTtujuan);
            this.bunifuCards1.Controls.Add(this.MTasal);
            this.bunifuCards1.Controls.Add(this.MTtanggal);
            this.bunifuCards1.Controls.Add(this.MTpemesan);
            this.bunifuCards1.Controls.Add(this.bunifuLabel5);
            this.bunifuCards1.Controls.Add(this.bunifuLabel4);
            this.bunifuCards1.Controls.Add(this.bunifuLabel3);
            this.bunifuCards1.Controls.Add(this.bunifuLabel2);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(120, 97);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(447, 200);
            this.bunifuCards1.TabIndex = 4;
            // 
            // MTtujuan
            // 
            this.MTtujuan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTtujuan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTtujuan.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTtujuan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTtujuan.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTtujuan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTtujuan.HintForeColor = System.Drawing.Color.Empty;
            this.MTtujuan.HintText = "";
            this.MTtujuan.isPassword = false;
            this.MTtujuan.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTtujuan.LineIdleColor = System.Drawing.Color.Gray;
            this.MTtujuan.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTtujuan.LineThickness = 3;
            this.MTtujuan.Location = new System.Drawing.Point(157, 148);
            this.MTtujuan.Margin = new System.Windows.Forms.Padding(4);
            this.MTtujuan.MaxLength = 32767;
            this.MTtujuan.Name = "MTtujuan";
            this.MTtujuan.Size = new System.Drawing.Size(209, 33);
            this.MTtujuan.TabIndex = 8;
            this.MTtujuan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTasal
            // 
            this.MTasal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTasal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTasal.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTasal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTasal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTasal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTasal.HintForeColor = System.Drawing.Color.Empty;
            this.MTasal.HintText = "";
            this.MTasal.isPassword = false;
            this.MTasal.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTasal.LineIdleColor = System.Drawing.Color.Gray;
            this.MTasal.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTasal.LineThickness = 3;
            this.MTasal.Location = new System.Drawing.Point(157, 107);
            this.MTasal.Margin = new System.Windows.Forms.Padding(4);
            this.MTasal.MaxLength = 32767;
            this.MTasal.Name = "MTasal";
            this.MTasal.Size = new System.Drawing.Size(209, 33);
            this.MTasal.TabIndex = 7;
            this.MTasal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTtanggal
            // 
            this.MTtanggal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTtanggal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTtanggal.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTtanggal.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTtanggal.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTtanggal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTtanggal.HintForeColor = System.Drawing.Color.Empty;
            this.MTtanggal.HintText = "";
            this.MTtanggal.isPassword = false;
            this.MTtanggal.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTtanggal.LineIdleColor = System.Drawing.Color.Gray;
            this.MTtanggal.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTtanggal.LineThickness = 3;
            this.MTtanggal.Location = new System.Drawing.Point(157, 66);
            this.MTtanggal.Margin = new System.Windows.Forms.Padding(4);
            this.MTtanggal.MaxLength = 32767;
            this.MTtanggal.Name = "MTtanggal";
            this.MTtanggal.Size = new System.Drawing.Size(209, 33);
            this.MTtanggal.TabIndex = 6;
            this.MTtanggal.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTpemesan
            // 
            this.MTpemesan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTpemesan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTpemesan.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTpemesan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTpemesan.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTpemesan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTpemesan.HintForeColor = System.Drawing.Color.Empty;
            this.MTpemesan.HintText = "";
            this.MTpemesan.isPassword = false;
            this.MTpemesan.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTpemesan.LineIdleColor = System.Drawing.Color.Gray;
            this.MTpemesan.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTpemesan.LineThickness = 3;
            this.MTpemesan.Location = new System.Drawing.Point(157, 25);
            this.MTpemesan.Margin = new System.Windows.Forms.Padding(4);
            this.MTpemesan.MaxLength = 32767;
            this.MTpemesan.Name = "MTpemesan";
            this.MTpemesan.Size = new System.Drawing.Size(209, 33);
            this.MTpemesan.TabIndex = 5;
            this.MTpemesan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // bunifuLabel5
            // 
            this.bunifuLabel5.AutoEllipsis = false;
            this.bunifuLabel5.CursorType = null;
            this.bunifuLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel5.Location = new System.Drawing.Point(26, 157);
            this.bunifuLabel5.Name = "bunifuLabel5";
            this.bunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel5.Size = new System.Drawing.Size(51, 22);
            this.bunifuLabel5.TabIndex = 4;
            this.bunifuLabel5.Text = "Tujuan";
            this.bunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.Location = new System.Drawing.Point(26, 116);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(34, 22);
            this.bunifuLabel4.TabIndex = 3;
            this.bunifuLabel4.Text = "Asal";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.Location = new System.Drawing.Point(26, 75);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(60, 22);
            this.bunifuLabel3.TabIndex = 2;
            this.bunifuLabel3.Text = "Tanggal";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(26, 34);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(70, 22);
            this.bunifuLabel2.TabIndex = 1;
            this.bunifuLabel2.Text = "Pemesan";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(105, 11);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(351, 40);
            this.bunifuLabel1.TabIndex = 2;
            this.bunifuLabel1.Text = "FAKTUR PEMESANAN";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.Location = new System.Drawing.Point(529, 20);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(24, 22);
            this.bunifuLabel6.TabIndex = 5;
            this.bunifuLabel6.Text = "ID. ";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bunifuImageButton2);
            this.panel1.Controls.Add(this.labelKode);
            this.panel1.Controls.Add(this.bunifuLabel1);
            this.panel1.Controls.Add(this.bunifuLabel6);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(651, 60);
            this.panel1.TabIndex = 6;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.ActiveImage = null;
            this.bunifuImageButton2.AllowAnimations = true;
            this.bunifuImageButton2.AllowBuffering = false;
            this.bunifuImageButton2.AllowZooming = false;
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.ErrorImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.ErrorImage")));
            this.bunifuImageButton2.FadeWhenInactive = false;
            this.bunifuImageButton2.Flip = Bunifu.UI.WinForms.BunifuImageButton.FlipOrientation.Normal;
            this.bunifuImageButton2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.ImageLocation = null;
            this.bunifuImageButton2.ImageMargin = 40;
            this.bunifuImageButton2.ImageSize = new System.Drawing.Size(30, 38);
            this.bunifuImageButton2.ImageZoomSize = new System.Drawing.Size(70, 78);
            this.bunifuImageButton2.InitialImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.InitialImage")));
            this.bunifuImageButton2.Location = new System.Drawing.Point(-1, -9);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Rotation = 0;
            this.bunifuImageButton2.ShowActiveImage = true;
            this.bunifuImageButton2.ShowCursorChanges = true;
            this.bunifuImageButton2.ShowImageBorders = false;
            this.bunifuImageButton2.ShowSizeMarkers = false;
            this.bunifuImageButton2.Size = new System.Drawing.Size(70, 78);
            this.bunifuImageButton2.TabIndex = 11;
            this.bunifuImageButton2.ToolTipText = "";
            this.bunifuImageButton2.WaitOnLoad = false;
            this.bunifuImageButton2.Zoom = 40;
            this.bunifuImageButton2.ZoomSpeed = 10;
            this.bunifuImageButton2.Click += new System.EventHandler(this.bunifuImageButton2_Click);
            // 
            // labelKode
            // 
            this.labelKode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelKode.AutoEllipsis = false;
            this.labelKode.CursorType = null;
            this.labelKode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKode.ForeColor = System.Drawing.Color.Red;
            this.labelKode.Location = new System.Drawing.Point(574, 20);
            this.labelKode.Name = "labelKode";
            this.labelKode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelKode.Size = new System.Drawing.Size(64, 22);
            this.labelKode.TabIndex = 6;
            this.labelKode.Text = "nomor id";
            this.labelKode.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.labelKode.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel8
            // 
            this.bunifuLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel8.AutoEllipsis = false;
            this.bunifuLabel8.CursorType = null;
            this.bunifuLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel8.Location = new System.Drawing.Point(120, 318);
            this.bunifuLabel8.Name = "bunifuLabel8";
            this.bunifuLabel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel8.Size = new System.Drawing.Size(218, 22);
            this.bunifuLabel8.TabIndex = 7;
            this.bunifuLabel8.Text = "telah memesan tiket sebanyak ";
            this.bunifuLabel8.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel8.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // Labeljumlahtiket
            // 
            this.Labeljumlahtiket.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Labeljumlahtiket.AutoEllipsis = false;
            this.Labeljumlahtiket.CursorType = null;
            this.Labeljumlahtiket.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Labeljumlahtiket.ForeColor = System.Drawing.Color.Red;
            this.Labeljumlahtiket.Location = new System.Drawing.Point(471, 318);
            this.Labeljumlahtiket.Name = "Labeljumlahtiket";
            this.Labeljumlahtiket.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Labeljumlahtiket.Size = new System.Drawing.Size(83, 22);
            this.Labeljumlahtiket.TabIndex = 8;
            this.Labeljumlahtiket.Text = "jumlah tiket";
            this.Labeljumlahtiket.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.Labeljumlahtiket.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // Labelharga
            // 
            this.Labelharga.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Labelharga.AutoEllipsis = false;
            this.Labelharga.CursorType = null;
            this.Labelharga.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Labelharga.ForeColor = System.Drawing.Color.Red;
            this.Labelharga.Location = new System.Drawing.Point(471, 350);
            this.Labelharga.Name = "Labelharga";
            this.Labelharga.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Labelharga.Size = new System.Drawing.Size(44, 22);
            this.Labelharga.TabIndex = 10;
            this.Labelharga.Text = "harga";
            this.Labelharga.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.Labelharga.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel11
            // 
            this.bunifuLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel11.AutoEllipsis = false;
            this.bunifuLabel11.CursorType = null;
            this.bunifuLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel11.Location = new System.Drawing.Point(120, 350);
            this.bunifuLabel11.Name = "bunifuLabel11";
            this.bunifuLabel11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel11.Size = new System.Drawing.Size(79, 22);
            this.bunifuLabel11.TabIndex = 9;
            this.bunifuLabel11.Text = "total harga";
            this.bunifuLabel11.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel11.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // FakturPemesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(651, 399);
            this.Controls.Add(this.Labelharga);
            this.Controls.Add(this.bunifuLabel11);
            this.Controls.Add(this.Labeljumlahtiket);
            this.Controls.Add(this.bunifuLabel8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bunifuCards1);
            this.Name = "FakturPemesan";
            this.Text = "s";
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTtujuan;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTasal;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTtanggal;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTpemesan;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel5;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuLabel labelKode;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel8;
        private Bunifu.UI.WinForms.BunifuLabel Labeljumlahtiket;
        private Bunifu.UI.WinForms.BunifuLabel Labelharga;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel11;
        private Bunifu.UI.WinForms.BunifuImageButton bunifuImageButton2;
    }
}
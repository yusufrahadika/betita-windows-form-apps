﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace Project_Akhir
{
    public partial class DataPemesan : Form
    {
        // Prepare the connection
        private MySqlConnection databaseConnection = Program.databaseConnection;
        private string id_perjalanan, asal, tujuan;
        public int count, harga_bisnis, harga_eksekutif, harga_ekonomi;

        public DataPemesan(string id_perjalanan, int count, int harga_ekonomi, int harga_bisnis, int harga_eksekutif, string asal, string tujuan)
        {
            InitializeComponent();
            this.id_perjalanan = id_perjalanan;
            MTNama.Text = Program.nama_depan + " " + Program.nama_belakang;
            MTNoHandphone.Text = Program.nomor_telepon;
            MTAlamat.Text = Program.alamat;
            MTEmail.Text = Program.email;

            this.harga_ekonomi = harga_ekonomi;
            this.harga_bisnis = harga_bisnis;
            this.harga_eksekutif = harga_eksekutif;
            this.count = count;

            this.asal = asal;
            this.tujuan = tujuan;

            for (int i = 1; i <= count; i++)
            {
                flowLayoutDataPenumpang.Controls.Add(new UserControlDataPemesan(i));
            }
        }

        private void btPesan_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < flowLayoutDataPenumpang.Controls.Count; i++)
            {
                UserControlDataPemesan currentControl = (UserControlDataPemesan)flowLayoutDataPenumpang.Controls[i];
                if (currentControl.nama == "" || currentControl.no_identitas == "")
                {
                    MessageBox.Show("Lengkapi data pengguna " + (i + 1) + "!");
                    return;
                }
            }

            string kode;
            int harga;

            Debug.Print(ddKelasKereta.Text);

            switch (ddKelasKereta.Text)
            {
                case "Eksekutif":
                    kode = "ex";
                    harga = harga_eksekutif;
                    break;
                case "Bisnis":
                    kode = "bu";
                    harga = harga_bisnis;
                    break;
                default:
                    kode = "ec";
                    harga = harga_ekonomi;
                    break;
            }

            databaseConnection.Open();
            MySqlTransaction mySqlTransaction = databaseConnection.BeginTransaction();
            try
            {
                Vlingo.UUID.RandomBasedGenerator rbg = new Vlingo.UUID.RandomBasedGenerator();

                string queryInsertPesanan = "INSERT INTO pesanan (id, username_pengguna, id_perjalanan) VALUES (@id, @username, @id_perjalanan)";
                MySqlCommand cmd = new MySqlCommand(queryInsertPesanan, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Transaction = mySqlTransaction;
                string id_pesanan = rbg.GenerateGuid().ToString();
                Debug.Print(id_pesanan);
                cmd.Parameters.AddWithValue("@id", id_pesanan);
                cmd.Parameters.AddWithValue("@username", Program.username);
                cmd.Parameters.AddWithValue("@id_perjalanan", id_perjalanan);
                cmd.ExecuteNonQuery();

                string queryCekKursi = "SELECT kode_kursi FROM perjalanan LEFT JOIN pesanan ON perjalanan.id = pesanan.id_perjalanan " +
                    "LEFT JOIN pesanan_perjalanan ON pesanan_perjalanan.id_pesanan = pesanan.id " +
                    "WHERE perjalanan.id = @id_perjalanan AND pesanan_perjalanan.kode_kursi LIKE @kode " +
                    "ORDER BY kode_kursi DESC LIMIT 1";
                MySqlCommand cmdKursi = new MySqlCommand(queryCekKursi, databaseConnection);
                cmdKursi.CommandTimeout = 60;
                cmdKursi.Transaction = mySqlTransaction;
                cmdKursi.Parameters.AddWithValue("@id_perjalanan", id_perjalanan);
                cmdKursi.Parameters.AddWithValue("@kode", kode + "%");

                int lastKursi = 1;
                object resultKursi = cmdKursi.ExecuteScalar();
                if (resultKursi != null)
                {
                    lastKursi = int.Parse(resultKursi.ToString().Replace(kode, "")) + 1;
                }

                for (int i = 0; i < flowLayoutDataPenumpang.Controls.Count; i++)
                {
                    UserControlDataPemesan currentControl = (UserControlDataPemesan)flowLayoutDataPenumpang.Controls[i];

                    string queryInsertPenumpang = "INSERT INTO pesanan_perjalanan (id, id_pesanan, kode_kursi, nama_penumpang, no_identitas_penumpang) VALUES (@id, @id_pesanan, @kode_kursi, @nama_penumpang, @no_id)";
                    MySqlCommand cmdPenumpang = new MySqlCommand(queryInsertPenumpang, databaseConnection);
                    cmdPenumpang.CommandTimeout = 60;
                    cmdPenumpang.Transaction = mySqlTransaction;
                    string id_penumpang = rbg.GenerateGuid().ToString();
                    Debug.Print(kode);
                    Debug.Print(harga.ToString());
                    cmdPenumpang.Parameters.AddWithValue("@id", id_penumpang);
                    cmdPenumpang.Parameters.AddWithValue("@id_pesanan", id_pesanan);
                    cmdPenumpang.Parameters.AddWithValue("@kode_kursi", kode + lastKursi.ToString("D4"));
                    cmdPenumpang.Parameters.AddWithValue("@nama_penumpang", currentControl.nama);
                    cmdPenumpang.Parameters.AddWithValue("@no_id", currentControl.no_identitas);
                    cmdPenumpang.ExecuteNonQuery();

                    lastKursi++;
                }
                MessageBox.Show("Pesanan berhasil ditambahkan");
                mySqlTransaction.Commit();
                Close();
                Pembayaran pembayaran = new Pembayaran(id_pesanan, count, harga);
                pembayaran.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                mySqlTransaction.Rollback();
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_Akhir
{
    public partial class UserControlMyTrip : UserControl
    {
        public UserControlMyTrip(
            string nama_kereta,
            string id_kereta,
            string kota_asal,
            string kota_tujuan,
            DateTime waktu_berangkat,
            DateTime waktu_sampai,
            string kode_pemesanan,
            string status
        )
        {
            InitializeComponent();
            labelNamaKereta.Text = nama_kereta + " (" + id_kereta + ")";
            labelTempatBerangkat.Text = kota_asal;
            labelTempatTujuan.Text = kota_tujuan;
            labelTanggalBerangkat.Text = waktu_berangkat.ToString("dd-MM-yyyy");
            labelWaktuBerangkat.Text = waktu_berangkat.ToString("t");
            labelTanggalTujuan.Text = waktu_sampai.ToString("dd-MM-yyyy");
            labelWaktuTujuan.Text = waktu_sampai.ToString("t");
            labelKodePemesanan.Text = kode_pemesanan.Substring(0, 8);
            button1.Text = status;
        }
    }
}

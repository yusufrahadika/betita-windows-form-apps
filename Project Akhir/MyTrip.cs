﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Project_Akhir
{
    public partial class MyTrip : Form
    {
        MySqlConnection databaseConnection = Program.databaseConnection;

        public MyTrip()
        {
            InitializeComponent();
            refreshAkanDatang();
            refreshSelesai();
        }

        public void refreshAkanDatang()
        {
            string query = "SELECT pesanan.id id_pesanan, id_perjalanan, status, perjalanan.kota_asal, perjalanan.kota_tujuan, perjalanan.waktu_berangkat, perjalanan.waktu_sampai, kereta.id id_kereta, kereta.nama nama_kereta FROM pesanan JOIN perjalanan ON pesanan.id_perjalanan = perjalanan.id JOIN kereta ON kereta.id = perjalanan.id_kereta WHERE username_pengguna = @username AND perjalanan.waktu_berangkat > CURRENT_DATE()";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@username", Program.username);
                MySqlDataReader reader = cmd.ExecuteReader();
                // IMPORTANT :
                // If your query returns result, use the following processor :
                flowLayoutPanel1.Controls.Clear();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        flowLayoutPanel1.Controls.Add(
                            new UserControlMyTrip(
                                reader["nama_kereta"].ToString(),
                                reader["id_kereta"].ToString(),
                                reader["kota_asal"].ToString(),
                                reader["kota_tujuan"].ToString(),
                                (DateTime)reader["waktu_berangkat"],
                                (DateTime)reader["waktu_sampai"],
                                reader["id_pesanan"].ToString(),
                                Convert.ToInt32(reader["status"]) == 0 ? "Belum Dibayar" : "Sudah Dibayar"
                            )
                        );
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        public void refreshSelesai()
        {
            string query = "SELECT pesanan.id id_pesanan, id_perjalanan, status, perjalanan.kota_asal, perjalanan.kota_tujuan, perjalanan.waktu_berangkat, perjalanan.waktu_sampai, kereta.id id_kereta, kereta.nama nama_kereta FROM pesanan JOIN perjalanan ON pesanan.id_perjalanan = perjalanan.id JOIN kereta ON kereta.id = perjalanan.id_kereta WHERE username_pengguna = @username AND perjalanan.waktu_berangkat <= CURRENT_DATE()";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@username", Program.username);
                MySqlDataReader reader = cmd.ExecuteReader();
                // IMPORTANT :
                // If your query returns result, use the following processor :
                flowLayoutPanel2.Controls.Clear();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        flowLayoutPanel2.Controls.Add(
                            new UserControlMyTrip(
                                reader["nama_kereta"].ToString(),
                                reader["id_kereta"].ToString(),
                                reader["kota_asal"].ToString(),
                                reader["kota_tujuan"].ToString(),
                                (DateTime)reader["waktu_berangkat"],
                                (DateTime)reader["waktu_sampai"],
                                reader["id_pesanan"].ToString(),
                                Convert.ToInt32(reader["status"]) == 0 ? "Dibatalkan" : (DateTime.Compare((DateTime)reader["waktu_sampai"], DateTime.Now) > 0 ? "Selesai" : "Dalam Perjalanan")
                            )
                        );
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                refreshAkanDatang();
            } else if (tabControl1.SelectedIndex == 1)
            {
                refreshSelesai();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Hide();
        }
    }
}

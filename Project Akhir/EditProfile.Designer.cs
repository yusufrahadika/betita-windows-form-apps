﻿namespace Project_Akhir
{
    partial class EditProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges borderEdges1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderEdges();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditProfile));
            this.bunifuShadowPanel1 = new Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel();
            this.bunifuLabel1 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.MTtelepon = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTalamat = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTemail = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTnamabelakang = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.MTnamadepan = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.btSimpan = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.bunifuLabel2 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel3 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel4 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel5 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuLabel6 = new Bunifu.UI.WinForms.BunifuLabel();
            this.bunifuImageButton1 = new Bunifu.UI.WinForms.BunifuImageButton();
            this.bunifuShadowPanel1.SuspendLayout();
            this.bunifuCards1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuShadowPanel1
            // 
            this.bunifuShadowPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuShadowPanel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.bunifuShadowPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuShadowPanel1.BorderColor = System.Drawing.Color.Gainsboro;
            this.bunifuShadowPanel1.Controls.Add(this.bunifuImageButton1);
            this.bunifuShadowPanel1.Controls.Add(this.bunifuLabel1);
            this.bunifuShadowPanel1.Location = new System.Drawing.Point(3, 2);
            this.bunifuShadowPanel1.Name = "bunifuShadowPanel1";
            this.bunifuShadowPanel1.PanelColor = System.Drawing.Color.Empty;
            this.bunifuShadowPanel1.ShadowDept = 2;
            this.bunifuShadowPanel1.ShadowTopLeftVisible = false;
            this.bunifuShadowPanel1.Size = new System.Drawing.Size(682, 66);
            this.bunifuShadowPanel1.TabIndex = 3;
            // 
            // bunifuLabel1
            // 
            this.bunifuLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel1.AutoEllipsis = false;
            this.bunifuLabel1.CursorType = null;
            this.bunifuLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel1.Location = new System.Drawing.Point(260, 10);
            this.bunifuLabel1.Name = "bunifuLabel1";
            this.bunifuLabel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel1.Size = new System.Drawing.Size(160, 40);
            this.bunifuLabel1.TabIndex = 2;
            this.bunifuLabel1.Text = "Edit Profile";
            this.bunifuLabel1.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel1.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.White;
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.LightCoral;
            this.bunifuCards1.Controls.Add(this.bunifuLabel6);
            this.bunifuCards1.Controls.Add(this.bunifuLabel5);
            this.bunifuCards1.Controls.Add(this.bunifuLabel4);
            this.bunifuCards1.Controls.Add(this.bunifuLabel3);
            this.bunifuCards1.Controls.Add(this.bunifuLabel2);
            this.bunifuCards1.Controls.Add(this.MTtelepon);
            this.bunifuCards1.Controls.Add(this.MTalamat);
            this.bunifuCards1.Controls.Add(this.MTemail);
            this.bunifuCards1.Controls.Add(this.MTnamabelakang);
            this.bunifuCards1.Controls.Add(this.MTnamadepan);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(112, 94);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(448, 305);
            this.bunifuCards1.TabIndex = 5;
            // 
            // MTtelepon
            // 
            this.MTtelepon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTtelepon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTtelepon.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTtelepon.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTtelepon.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTtelepon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTtelepon.HintForeColor = System.Drawing.Color.Empty;
            this.MTtelepon.HintText = "";
            this.MTtelepon.isPassword = false;
            this.MTtelepon.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTtelepon.LineIdleColor = System.Drawing.Color.Gray;
            this.MTtelepon.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTtelepon.LineThickness = 3;
            this.MTtelepon.Location = new System.Drawing.Point(204, 203);
            this.MTtelepon.Margin = new System.Windows.Forms.Padding(4);
            this.MTtelepon.MaxLength = 32767;
            this.MTtelepon.Name = "MTtelepon";
            this.MTtelepon.Size = new System.Drawing.Size(209, 33);
            this.MTtelepon.TabIndex = 15;
            this.MTtelepon.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTalamat
            // 
            this.MTalamat.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTalamat.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTalamat.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTalamat.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTalamat.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTalamat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTalamat.HintForeColor = System.Drawing.Color.Empty;
            this.MTalamat.HintText = "";
            this.MTalamat.isPassword = false;
            this.MTalamat.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTalamat.LineIdleColor = System.Drawing.Color.Gray;
            this.MTalamat.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTalamat.LineThickness = 3;
            this.MTalamat.Location = new System.Drawing.Point(204, 157);
            this.MTalamat.Margin = new System.Windows.Forms.Padding(4);
            this.MTalamat.MaxLength = 32767;
            this.MTalamat.Name = "MTalamat";
            this.MTalamat.Size = new System.Drawing.Size(209, 33);
            this.MTalamat.TabIndex = 8;
            this.MTalamat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTemail
            // 
            this.MTemail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTemail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTemail.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTemail.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTemail.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTemail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTemail.HintForeColor = System.Drawing.Color.Empty;
            this.MTemail.HintText = "";
            this.MTemail.isPassword = false;
            this.MTemail.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTemail.LineIdleColor = System.Drawing.Color.Gray;
            this.MTemail.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTemail.LineThickness = 3;
            this.MTemail.Location = new System.Drawing.Point(204, 116);
            this.MTemail.Margin = new System.Windows.Forms.Padding(4);
            this.MTemail.MaxLength = 32767;
            this.MTemail.Name = "MTemail";
            this.MTemail.Size = new System.Drawing.Size(209, 33);
            this.MTemail.TabIndex = 7;
            this.MTemail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTnamabelakang
            // 
            this.MTnamabelakang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTnamabelakang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTnamabelakang.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTnamabelakang.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTnamabelakang.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTnamabelakang.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTnamabelakang.HintForeColor = System.Drawing.Color.Empty;
            this.MTnamabelakang.HintText = "";
            this.MTnamabelakang.isPassword = false;
            this.MTnamabelakang.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTnamabelakang.LineIdleColor = System.Drawing.Color.Gray;
            this.MTnamabelakang.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTnamabelakang.LineThickness = 3;
            this.MTnamabelakang.Location = new System.Drawing.Point(204, 75);
            this.MTnamabelakang.Margin = new System.Windows.Forms.Padding(4);
            this.MTnamabelakang.MaxLength = 32767;
            this.MTnamabelakang.Name = "MTnamabelakang";
            this.MTnamabelakang.Size = new System.Drawing.Size(209, 33);
            this.MTnamabelakang.TabIndex = 6;
            this.MTnamabelakang.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // MTnamadepan
            // 
            this.MTnamadepan.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.MTnamadepan.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.MTnamadepan.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.MTnamadepan.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.MTnamadepan.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.MTnamadepan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.MTnamadepan.HintForeColor = System.Drawing.Color.Empty;
            this.MTnamadepan.HintText = "";
            this.MTnamadepan.isPassword = false;
            this.MTnamadepan.LineFocusedColor = System.Drawing.Color.Blue;
            this.MTnamadepan.LineIdleColor = System.Drawing.Color.Gray;
            this.MTnamadepan.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.MTnamadepan.LineThickness = 3;
            this.MTnamadepan.Location = new System.Drawing.Point(204, 34);
            this.MTnamadepan.Margin = new System.Windows.Forms.Padding(4);
            this.MTnamadepan.MaxLength = 32767;
            this.MTnamadepan.Name = "MTnamadepan";
            this.MTnamadepan.Size = new System.Drawing.Size(209, 33);
            this.MTnamadepan.TabIndex = 5;
            this.MTnamadepan.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // btSimpan
            // 
            this.btSimpan.AllowToggling = false;
            this.btSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btSimpan.AnimationSpeed = 200;
            this.btSimpan.AutoGenerateColors = false;
            this.btSimpan.BackColor = System.Drawing.Color.Transparent;
            this.btSimpan.BackColor1 = System.Drawing.Color.White;
            this.btSimpan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btSimpan.BackgroundImage")));
            this.btSimpan.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btSimpan.ButtonText = "Simpan";
            this.btSimpan.ButtonTextMarginLeft = 0;
            this.btSimpan.ColorContrastOnClick = 45;
            this.btSimpan.ColorContrastOnHover = 45;
            this.btSimpan.Cursor = System.Windows.Forms.Cursors.Hand;
            borderEdges1.BottomLeft = true;
            borderEdges1.BottomRight = true;
            borderEdges1.TopLeft = true;
            borderEdges1.TopRight = true;
            this.btSimpan.CustomizableEdges = borderEdges1;
            this.btSimpan.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btSimpan.DisabledBorderColor = System.Drawing.Color.Empty;
            this.btSimpan.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.btSimpan.DisabledForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(160)))), ((int)(((byte)(168)))));
            this.btSimpan.FocusState = Bunifu.UI.WinForms.BunifuButton.BunifuButton.ButtonStates.Pressed;
            this.btSimpan.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSimpan.ForeColor = System.Drawing.Color.Black;
            this.btSimpan.IconLeftCursor = System.Windows.Forms.Cursors.Hand;
            this.btSimpan.IconMarginLeft = 11;
            this.btSimpan.IconPadding = 10;
            this.btSimpan.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btSimpan.IdleBorderColor = System.Drawing.Color.White;
            this.btSimpan.IdleBorderRadius = 3;
            this.btSimpan.IdleBorderThickness = 1;
            this.btSimpan.IdleFillColor = System.Drawing.Color.White;
            this.btSimpan.IdleIconLeftImage = null;
            this.btSimpan.IdleIconRightImage = null;
            this.btSimpan.IndicateFocus = false;
            this.btSimpan.Location = new System.Drawing.Point(281, 420);
            this.btSimpan.Name = "btSimpan";
            this.btSimpan.onHoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btSimpan.onHoverState.BorderRadius = 3;
            this.btSimpan.onHoverState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btSimpan.onHoverState.BorderThickness = 1;
            this.btSimpan.onHoverState.FillColor = System.Drawing.Color.White;
            this.btSimpan.onHoverState.ForeColor = System.Drawing.Color.Black;
            this.btSimpan.onHoverState.IconLeftImage = null;
            this.btSimpan.onHoverState.IconRightImage = null;
            this.btSimpan.OnIdleState.BorderColor = System.Drawing.Color.White;
            this.btSimpan.OnIdleState.BorderRadius = 3;
            this.btSimpan.OnIdleState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btSimpan.OnIdleState.BorderThickness = 1;
            this.btSimpan.OnIdleState.FillColor = System.Drawing.Color.White;
            this.btSimpan.OnIdleState.ForeColor = System.Drawing.Color.Black;
            this.btSimpan.OnIdleState.IconLeftImage = null;
            this.btSimpan.OnIdleState.IconRightImage = null;
            this.btSimpan.OnPressedState.BorderColor = System.Drawing.Color.Transparent;
            this.btSimpan.OnPressedState.BorderRadius = 3;
            this.btSimpan.OnPressedState.BorderStyle = Bunifu.UI.WinForms.BunifuButton.BunifuButton.BorderStyles.Solid;
            this.btSimpan.OnPressedState.BorderThickness = 1;
            this.btSimpan.OnPressedState.FillColor = System.Drawing.Color.White;
            this.btSimpan.OnPressedState.ForeColor = System.Drawing.Color.Black;
            this.btSimpan.OnPressedState.IconLeftImage = null;
            this.btSimpan.OnPressedState.IconRightImage = null;
            this.btSimpan.Size = new System.Drawing.Size(125, 42);
            this.btSimpan.TabIndex = 9;
            this.btSimpan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btSimpan.TextMarginLeft = 0;
            this.btSimpan.UseDefaultRadiusAndThickness = true;
            this.btSimpan.Click += new System.EventHandler(this.btSimpan_Click);
            // 
            // bunifuLabel2
            // 
            this.bunifuLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel2.AutoEllipsis = false;
            this.bunifuLabel2.CursorType = null;
            this.bunifuLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel2.Location = new System.Drawing.Point(31, 45);
            this.bunifuLabel2.Name = "bunifuLabel2";
            this.bunifuLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel2.Size = new System.Drawing.Size(97, 22);
            this.bunifuLabel2.TabIndex = 16;
            this.bunifuLabel2.Text = "Nama Depan";
            this.bunifuLabel2.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel2.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel3
            // 
            this.bunifuLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel3.AutoEllipsis = false;
            this.bunifuLabel3.CursorType = null;
            this.bunifuLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel3.Location = new System.Drawing.Point(31, 86);
            this.bunifuLabel3.Name = "bunifuLabel3";
            this.bunifuLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel3.Size = new System.Drawing.Size(116, 22);
            this.bunifuLabel3.TabIndex = 17;
            this.bunifuLabel3.Text = "Nama Belakang";
            this.bunifuLabel3.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel3.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel4
            // 
            this.bunifuLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel4.AutoEllipsis = false;
            this.bunifuLabel4.CursorType = null;
            this.bunifuLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel4.Location = new System.Drawing.Point(31, 127);
            this.bunifuLabel4.Name = "bunifuLabel4";
            this.bunifuLabel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel4.Size = new System.Drawing.Size(42, 22);
            this.bunifuLabel4.TabIndex = 18;
            this.bunifuLabel4.Text = "Email";
            this.bunifuLabel4.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel4.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel5
            // 
            this.bunifuLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel5.AutoEllipsis = false;
            this.bunifuLabel5.CursorType = null;
            this.bunifuLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel5.Location = new System.Drawing.Point(31, 168);
            this.bunifuLabel5.Name = "bunifuLabel5";
            this.bunifuLabel5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel5.Size = new System.Drawing.Size(53, 22);
            this.bunifuLabel5.TabIndex = 19;
            this.bunifuLabel5.Text = "Alamat";
            this.bunifuLabel5.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel5.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuLabel6
            // 
            this.bunifuLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuLabel6.AutoEllipsis = false;
            this.bunifuLabel6.CursorType = null;
            this.bunifuLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuLabel6.Location = new System.Drawing.Point(31, 214);
            this.bunifuLabel6.Name = "bunifuLabel6";
            this.bunifuLabel6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.bunifuLabel6.Size = new System.Drawing.Size(88, 22);
            this.bunifuLabel6.TabIndex = 20;
            this.bunifuLabel6.Text = "No. Telepon";
            this.bunifuLabel6.TextAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.bunifuLabel6.TextFormat = Bunifu.UI.WinForms.BunifuLabel.TextFormattingOptions.Default;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.ActiveImage = null;
            this.bunifuImageButton1.AllowAnimations = true;
            this.bunifuImageButton1.AllowBuffering = false;
            this.bunifuImageButton1.AllowZooming = false;
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.ErrorImage")));
            this.bunifuImageButton1.FadeWhenInactive = false;
            this.bunifuImageButton1.Flip = Bunifu.UI.WinForms.BunifuImageButton.FlipOrientation.Normal;
            this.bunifuImageButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.ImageLocation = null;
            this.bunifuImageButton1.ImageMargin = 40;
            this.bunifuImageButton1.ImageSize = new System.Drawing.Size(28, 26);
            this.bunifuImageButton1.ImageZoomSize = new System.Drawing.Size(68, 66);
            this.bunifuImageButton1.InitialImage = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.InitialImage")));
            this.bunifuImageButton1.Location = new System.Drawing.Point(0, -3);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Rotation = 90;
            this.bunifuImageButton1.ShowActiveImage = true;
            this.bunifuImageButton1.ShowCursorChanges = true;
            this.bunifuImageButton1.ShowImageBorders = false;
            this.bunifuImageButton1.ShowSizeMarkers = false;
            this.bunifuImageButton1.Size = new System.Drawing.Size(68, 66);
            this.bunifuImageButton1.TabIndex = 10;
            this.bunifuImageButton1.ToolTipText = "";
            this.bunifuImageButton1.WaitOnLoad = false;
            this.bunifuImageButton1.Zoom = 40;
            this.bunifuImageButton1.ZoomSpeed = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // EditProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(681, 490);
            this.Controls.Add(this.btSimpan);
            this.Controls.Add(this.bunifuCards1);
            this.Controls.Add(this.bunifuShadowPanel1);
            this.Name = "EditProfile";
            this.Text = "EditProfile";
            this.bunifuShadowPanel1.ResumeLayout(false);
            this.bunifuShadowPanel1.PerformLayout();
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.UI.WinForm.BunifuShadowPanel.BunifuShadowPanel bunifuShadowPanel1;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTtelepon;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTalamat;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTemail;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTnamabelakang;
        private Bunifu.Framework.UI.BunifuMaterialTextbox MTnamadepan;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btSimpan;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel6;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel5;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel4;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel3;
        private Bunifu.UI.WinForms.BunifuLabel bunifuLabel2;
        private Bunifu.UI.WinForms.BunifuImageButton bunifuImageButton1;
    }
}
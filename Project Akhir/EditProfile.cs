﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace Project_Akhir
{
    public partial class EditProfile : Form
    {

        private MySqlConnection databaseConnection = Program.databaseConnection;

        Home frmParent = null;

        public EditProfile(Home home)
        {
            InitializeComponent();
            frmParent = home;
            
            MTnamadepan.Text = Program.nama_depan;
            MTnamabelakang.Text = Program.nama_belakang;
            MTemail.Text = Program.email;
            MTalamat.Text = Program.alamat;
            MTtelepon.Text = Program.nomor_telepon;
        }

        private void btSimpan_Click(object sender, EventArgs e)
        {
            string query = "UPDATE pengguna SET nama_depan = @nama_depan, nama_belakang = @nama_belakang, " +
               "email = @email, alamat = @alamat, nomor_telepon = @nomor_telepon WHERE username = @username";
            try
            {
                // Open the database
                databaseConnection.Open();
                MySqlCommand cmd = new MySqlCommand(query, databaseConnection);
                cmd.CommandTimeout = 60;
                cmd.Parameters.AddWithValue("@nama_depan", MTnamadepan.Text);
                cmd.Parameters.AddWithValue("@nama_belakang", MTnamabelakang.Text);
                cmd.Parameters.AddWithValue("@email", MTemail.Text);
                //cmd.Parameters.AddWithValue("@password", BCrypt.Net.BCrypt.HashPassword(tbPassword.Text));
                cmd.Parameters.AddWithValue("@alamat", MTalamat.Text);
                cmd.Parameters.AddWithValue("@nomor_telepon", MTtelepon.Text);
                cmd.Parameters.AddWithValue("@username", Program.username);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Update successful!");
                Home home = new Home();
                home.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                databaseConnection.Close();
            }
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            frmParent.Show();
            this.Hide();
        }
    }
}

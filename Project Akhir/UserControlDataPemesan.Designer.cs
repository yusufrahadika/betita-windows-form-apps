﻿namespace Project_Akhir
{
    partial class UserControlDataPemesan
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbNoIdentitas = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbNama = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.bunifuCustomLabel7 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel8 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox2.Controls.Add(this.tbNoIdentitas);
            this.groupBox2.Controls.Add(this.tbNama);
            this.groupBox2.Controls.Add(this.bunifuCustomLabel7);
            this.groupBox2.Controls.Add(this.bunifuCustomLabel8);
            this.groupBox2.Font = new System.Drawing.Font("Malgun Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(531, 192);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Penumpang";
            // 
            // tbNoIdentitas
            // 
            this.tbNoIdentitas.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNoIdentitas.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNoIdentitas.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNoIdentitas.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNoIdentitas.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNoIdentitas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNoIdentitas.HintForeColor = System.Drawing.Color.Empty;
            this.tbNoIdentitas.HintText = "";
            this.tbNoIdentitas.isPassword = false;
            this.tbNoIdentitas.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNoIdentitas.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNoIdentitas.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNoIdentitas.LineThickness = 3;
            this.tbNoIdentitas.Location = new System.Drawing.Point(216, 93);
            this.tbNoIdentitas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbNoIdentitas.MaxLength = 32767;
            this.tbNoIdentitas.Name = "tbNoIdentitas";
            this.tbNoIdentitas.Size = new System.Drawing.Size(293, 32);
            this.tbNoIdentitas.TabIndex = 36;
            this.tbNoIdentitas.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbNoIdentitas.OnValueChanged += new System.EventHandler(this.tbNoIdentitas_OnValueChanged);
            // 
            // tbNama
            // 
            this.tbNama.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.tbNama.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.tbNama.characterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.tbNama.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbNama.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.tbNama.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tbNama.HintForeColor = System.Drawing.Color.Empty;
            this.tbNama.HintText = "";
            this.tbNama.isPassword = false;
            this.tbNama.LineFocusedColor = System.Drawing.Color.Blue;
            this.tbNama.LineIdleColor = System.Drawing.Color.Gray;
            this.tbNama.LineMouseHoverColor = System.Drawing.Color.Blue;
            this.tbNama.LineThickness = 3;
            this.tbNama.Location = new System.Drawing.Point(216, 55);
            this.tbNama.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbNama.MaxLength = 32767;
            this.tbNama.Name = "tbNama";
            this.tbNama.Size = new System.Drawing.Size(293, 30);
            this.tbNama.TabIndex = 35;
            this.tbNama.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.tbNama.OnValueChanged += new System.EventHandler(this.tbNama_OnValueChanged);
            // 
            // bunifuCustomLabel7
            // 
            this.bunifuCustomLabel7.AutoSize = true;
            this.bunifuCustomLabel7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel7.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel7.Location = new System.Drawing.Point(32, 104);
            this.bunifuCustomLabel7.Name = "bunifuCustomLabel7";
            this.bunifuCustomLabel7.Size = new System.Drawing.Size(69, 21);
            this.bunifuCustomLabel7.TabIndex = 32;
            this.bunifuCustomLabel7.Text = "No. KTP";
            // 
            // bunifuCustomLabel8
            // 
            this.bunifuCustomLabel8.AutoSize = true;
            this.bunifuCustomLabel8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuCustomLabel8.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel8.Location = new System.Drawing.Point(32, 64);
            this.bunifuCustomLabel8.Name = "bunifuCustomLabel8";
            this.bunifuCustomLabel8.Size = new System.Drawing.Size(52, 21);
            this.bunifuCustomLabel8.TabIndex = 31;
            this.bunifuCustomLabel8.Text = "Nama";
            // 
            // UserControlDataPemesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "UserControlDataPemesan";
            this.Size = new System.Drawing.Size(542, 207);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNoIdentitas;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbNama;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel7;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel8;
    }
}
